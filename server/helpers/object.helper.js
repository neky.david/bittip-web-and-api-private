module.exports = {

    /**
     * make non object array to array of Object
     */
    toListOfObject(data, Object) {
        var array = [];
        data.forEach(userData => {
            array.push(new Object(userData));
        });
        return array;
    },

    objectPropsToArray(obj) {
        return Object.keys(obj)
            .map(function (key) {
                return obj[key];
            });
    }

}