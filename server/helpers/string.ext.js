module.exports = {
    config: () => {

        Object.defineProperty(String.prototype, "toUnderscore", {
            value: function toUnderscore() {
                return this.split(/(?=[A-Z])/).join('_').toLowerCase();
            },
            writable: true,
            configurable: true
        });

        Object.defineProperty(String.prototype, "toCamel", {
            value: function toCamel() {
                return this.replace(/([-_]\w)/g, g => g[1].toUpperCase())
            },
            writable: true,
            configurable: true
        });

        
        var translate_re = /&(nbsp|amp|quot|lt|gt);/g;
        var translate = {
            "nbsp": " ",
            "amp": "&",
            "quot": "\"",
            "lt": "<",
            "gt": ">"
        };

        Object.defineProperty(String.prototype, "htmlToPlain", {
            value: function htmlToPlain() {
                return this.replace(translate_re, function (match, entity) {
                    return translate[entity];
                }).replace(/&#(\d+);/gi, function (match, numStr) {
                    var num = parseInt(numStr, 10);
                    return String.fromCharCode(num);
                }).replace(/<[^>]+>/g, '');
            },
            writable: true,
            configurable: true
        });

    }

}