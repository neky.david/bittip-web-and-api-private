/**
 * formatting function to pad numbers to two digits…
 **/
function twoDigits(d) {
    if (0 <= d && d < 10) return "0" + d.toString();
    if (-10 < d && d < 0) return "-0" + (-1 * d).toString();
    return d.toString();
}


module.exports = {
    config: () => {

        Object.defineProperty(Date.prototype, "toSqlFormat", {
            value: function toSqlFormat() {
                return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " +
                    twoDigits(this.getUTCHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
            },
            writable: true,
            configurable: true
        });

        Date.prototype.addHours = function (h) {

        };

        Object.defineProperty(Date.prototype, "addHours", {
            value: function addHours(hour) {
                this.setTime(this.getTime() + (hour * 60 * 60 * 1000));
                return this;
            },
            writable: true,
            configurable: true
        });

    }
}
