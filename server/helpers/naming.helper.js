function singleObjectNamesToUnderscore(obj) {
    newObject = {};

    for (var prop in obj) {
        newObject[prop.toUnderscore()] = obj[prop];
    }
    return newObject;
}

function singleObjectNamesToCamel(obj) {
    newObject = {};
    for (var prop in obj) {
        newObject[prop.toCamel()] = obj[prop];
    }
    return newObject;
}

function objectNamesTo(objects, namingFunc) {
    if (objects && typeof objects === 'object') {
        if (objects.length == undefined)
            return namingFunc(objects);

        for (var i = 0; i < objects.length; i++) {
            var obj = objects[i];
            if (typeof obj === 'object' && obj !== null)
                objects[i] = namingFunc(obj);
        }
    }
    return objects;
}

function objectNamesToUnderscore(obj) {
    return objectNamesTo(obj, singleObjectNamesToUnderscore);
}

function objectNamesToCamel(obj) {
    return objectNamesTo(obj, singleObjectNamesToCamel);
}

module.exports = {
    objectNamesToUnderscore,
    objectNamesToCamel
}