const morgan = require('morgan');
const invoiceTracker = require('../services/lightning/invoice.tracker');
const expressLoader = require('./express.loader');

module.exports = (app) => {

    //Extensions
    require('../helpers/string.ext').config();
    require('../helpers/date.ext').config();

    //logging
    if (process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));
    }

    //DB connect
    require('../services/db.service');

    //EXPRESS
    expressLoader(app);
    console.log('Express Initialized');

    //LND Invoice tracker
    //TODO: zjistit poslední addIndex, který jsme odchytili
    invoiceTracker.run();

}



