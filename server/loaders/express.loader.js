const express = require('express');
var cookieParser = require('cookie-parser')
const passport = require('passport');

module.exports = (app) => {

    //Json parsing
    app.use(express.json()); //{inflate: true}
    //parse the URL encoded body
    app.use(express.urlencoded({ extended: true }));
    app.use(cookieParser());

    // Add headers
    app.use(function (req, res, next) {
        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', process.env.WEB_URL);

        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);

        // Pass to next layer of middleware
        next();
    });

    // Passport middleware
    app.use(passport.initialize());

    //Route Middlewares
    app.use('/auth', require('../model/auth/auth.routes'));
    app.use('/users', require('../model/users/users.routes'));
    app.use('/profiles', require('../model/profiles/profiles.routes'));
    app.use('/files', require('../model/files/file.routes'));
    app.use('/tips', require('../model/tips/tips.router'));
    app.use('/wallets', require('../model/wallets/wallets.router'));

    app.use('/youtube', require('../model/platforms/youtube/youtube.routes'));
    app.use('/stackexchange', require('../model/platforms/stackexchange/stackexchange.routes'));
    app.use('/medium', require('../model/platforms/medium/medium.routes'));

    //Error handle middleware
    app.use(require('../errors/error.handler'));
}