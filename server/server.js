require('dotenv').config();
const express = require('express');
const loaders = require('./loaders/base.loader');

const app = express();
const port = process.env.PORT || 3000;

loaders(app);

const auth = require('./model/auth/auth.jwt');

app.get('/test', auth.auth, (req, res) => {
    res.send('SUCCESS!');
});

module.exports = app.listen(port, console.log(`BitTip server run on port ${port}`));
