const { HttpStatusCode } = require('./error.enums');
const logger = require('./Logger');
const UserError = require('./UserError');

/**
 * 
 * @param {Error} err 
 * @param {*} res 
 */
module.exports = async function (err, req, res, next) {
    await logger.error(
        err.message,
        err,
    );

    console.log('ERROR');
    console.log(err);

    if (err instanceof UserError) {
        res.status(err.httpCode);
        res.send({
            error: {
                message: err.message,
                status: err.httpCode,
                type: err.type,
                title: err.title,
                viewable: true
            }
        });
    }
    else {
        res.status(500);
        res.send({
            error: {
                message: 'An unexpected error occured. Try it again later.',
                status: HttpStatusCode.INTERNAL_SERVER,
                type: 'error',
                title: 'Internal Error',
                viewable: false
            }
        });
    }

    //await sendMailToAdminIfCritical();
    //await sendEventsToSentry();
}