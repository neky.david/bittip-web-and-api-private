module.exports = {
    HttpStatusCode: Object.freeze({ 
        //'OK': 200, 
        'BAD_REQUEST': 400, 
        'UNAUTHORIZED': 401,
        'FORBIDDEN': 403,
        'NOT_FOUND': 404, 
        'INTERNAL_SERVER': 500
     }),

     ErrorType: Object.freeze({ 
        'ERROR': 'error', 
        'WARNING': 'warn', 
        'INFO': 'info'
     })
}