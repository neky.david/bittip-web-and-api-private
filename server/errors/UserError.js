const {HttpStatusCode, ErrorType} = require('./error.enums');

class UserError extends Error {

    /**
     * 
     * @param {string} message 
     * @param {HttpStatusCode} httpCode 
     * @param {String} title
     * @param {ErrorType} type
     */
    constructor(message, httpCode, title, type) {
        super(message);
        Object.setPrototypeOf(this, new.target.prototype);

        this.httpCode = httpCode ? httpCode : HttpStatusCode.INTERNAL_SERVER;
        this.type = type ? type : ErrorType.ERROR;
        this.title = title;
        
        //this.isOperational = isOperational;

        Error.captureStackTrace(this);
    }
}

//free to extend the BaseError
//    class APIError extends BaseError {
//     constructor(name, httpCode = HttpStatusCode.INTERNAL_SERVER, isOperational = true, description = 'internal server error') {
//       super(name, httpCode, isOperational, description);
//     }
//    }

module.exports = UserError;