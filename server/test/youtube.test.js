const chai = require('chai');
const chaiHttp = require('chai-http');
const profilesService = require('../model/profiles/profiles.service');
const service = require('../model/platforms/youtube/youtube.service');

const expect = chai.expect;
chai.use(chaiHttp);

var googleDevelopersId = 'UC_x5XG1OV2P6uZZ5FSM9Ttw';
var accessToken;

describe('YOUTUBE', () => {

    it('It should refresh the ACCESS TOKEN', async () => {
        var refreshToken = (await profilesService.findOne(105232627656645289230, 'google')).refreshToken;
        [accessToken, expireDate] = await service.refreshAccessToken(refreshToken);
        expect(accessToken).to.not.be.null;
        expect(expireDate).to.not.be.null;
    });

    // it("Is should get ACCESS TOKEN from db", async () => {
    //     accessToken = (await profilesService.findOne(105232627656645289230, 'google')).accessToken;
    //     expect(accessToken).to.not.be.null;
    //     expect(accessToken).to.be.a('string');
    // });

    it("Is should get list of user's CHANNELS", async () => {
        var channels = await service.getChannels(accessToken);
        console.log(channels);
        expect(channels).to.be.a('array');
        expect(channels.length).to.be.above(0);
    });

    it('It should get VIDEO details by id', async () => {
        //var videoId = 'Ks-_Mh1QhMc';
        var videoId = 'IfoKgBb7UeM';
        var video = await service.getVideoById(videoId);
        console.log(video);
        expect(video).to.not.be.null;
        expect(video).to.be.a('object');
        expect(video).to.have.property('publicId');
        expect(video.publicId).to.be.eq(videoId);
    });

    it('It should list of VIDEOS from PLAYLIST', async () => {
        var playlistId = 'UU7rpfL8S8p3i65vvcd3E83w'; //BitTip
        //var playlistId = 'UU_x5XG1OV2P6uZZ5FSM9Ttw';
        var videos = await service.getVideosByPlaylistId(playlistId, accessToken);
        console.log(videos);
        expect(videos).to.be.a('array');
        expect(videos.length).to.be.above(0);
    });

});