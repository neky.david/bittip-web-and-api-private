const chai = require('chai');
const chaiHttp = require('chai-http');
const { expression } = require('joi');
const service = require('../model/platforms/medium/medium.service');

const expect = chai.expect;
chai.use(chaiHttp);

var integrationToken = process.env.MEDIUM_INTEGRATION_TOKEN;
var mediumUserId;
var publicationId;

describe('MEDIUM', function() {
    this.timeout(50000);

    // it("Is should get MEDIUM ACCOUNT from integration token", async () => {
    //     var account = await service.getAccount(integrationToken);
    //     console.log(account);
    //     expect(account).to.not.be.null;
    //     expect(account).to.be.a('object');
    //     expect(account).to.have.property('id');
    //     expect(account.id).to.not.be.null;
    //     mediumUserId = account.id;
    // });

    // it("It should get user's PUBLICATIONS", async () => {
    //     var publications = await service.getPublications(mediumUserId, integrationToken);
    //     expect(publications).to.not.be.null;
    //     expect(publications).to.be.a('array');
    //     expect(publications.length).to.be.above(0);
    //     publicationId = publications[0].id;
    // });

    // it("It should get publication's CONTRIBUTORS", async () => {
    //     var contributors = await service.getContributorsByPublicationId(mediumUserId, integrationToken);
    //     expect(contributors).to.not.be.null;
    //     expect(contributors).to.be.a('array');
    //     expect(contributors.length).to.be.above(0);
    // });

    it("It should get POST DETAIL from URL", async() => {
        //var url = 'https://levelup.gitconnected.com/serving-100k-requests-second-from-a-fanless-raspberry-pi-4-over-ethernet-fdd2c2e05a1e';
        var url = 'https://medium.com/a/fdd2c2e05a1e';
        var post = await service.getPostDetailFromUrl(url);
        console.log(post);
        //console.log(post.toContent());
        expect(post).to.be.a('object');
        expect(post.id).to.be.not.null;
        expect(post.authorUrl).to.be.not.null;
    })

});