const chai = require('chai');
const chaiHttp = require('chai-http');
const userService = require('../model/users/users.service');

const expect = chai.expect;
chai.use(chaiHttp);
   
describe('EXTENSION_PARAMS', () => {

    it("It should SET and GET EXTENSION PARAM to user", async () => {
        var number = Math.random();
        var user = (await userService.list())[0];
        var newParam = await user.setExtParam("TEST", number);
        var dbParam = await user.getExtParam("TEST");
        expect(dbParam).to.not.be.null;
        expect(dbParam).to.be.a('Object');
        expect(dbParam).to.have.property('value');
        expect(dbParam.value).to.be.eq(number.toString());
    });

});