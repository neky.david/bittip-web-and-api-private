const server = require('../server');
const db = require('../services/db.service');

function checkConnection(done) {
    if (db.isConnected())
        done();
    setTimeout(() => { checkConnection(done) }, 5000);
}

before(function (done) {
    this.timeout(100000);
    checkConnection(done);
});