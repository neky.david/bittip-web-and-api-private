const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const model = require('../model/users/users.model');
const service = require('../model/users/users.service');
const User = require('../model/users/User');

const expect = chai.expect;
chai.use(chaiHttp);


describe('USERS', () => {

    //DB ACCESS
    describe('Users DB Access', () => {
        it('Is should get ALL USERS', async () => {
            var users = await model.getAll();
            expect(users).to.be.a('array');
            expect(users.length).to.be.above(0);
        });
        it('Is should get ONE USER by id', async () => {
            var userId = 26;
            var user = await model.getById(userId);
            expect(user).to.be.a('object');
            expect(user).to.have.property('id');
            expect(user.id).to.be.eq(userId);
        });
        it('Is should get ONE USER by email', async () => {
            var user = await model.getByEmail('neky.david@gmail.com');
            expect(user).to.be.a('object');
            expect(user).to.have.property('id');
            expect(user.id).to.be.above(0);
        });
    });

    //API
    describe('Users API', () => {

        describe('GET /users', () => {
            it('It should FORBIDDEN to unauthorized users', () => {
                chai.request(server)
                    .get('/users')
                    .end((err, res) => {
                        expect(res).to.have.status(401);
                    })
            });

            // it('It should GET all users (ADMIN)', () => {
            //     chai.request(server)
            //         .get('/users')
            //         .set('Authorization', 'Bearer ' + process.env.JWT_TOKEN_ADMIN)
            //         .end((err, res) => {
            //             res.to.have.status(200);
            //             res.body.to.be.a('array');
            //             res.body.length.to.be.above(0);
            //         })
            // });

            it('It should GET ONE user - only current user (USER)', () => {
                chai.request(server)
                    .get('/users')
                    .set('Authorization', 'Bearer ' + process.env.JWT_TOKEN_USER)
                    .end((err, res) => {
                        expect(res).to.have.status(200);
                        expect(res.body).to.be.a('array');
                        expect(res.body.length).to.be.eq(1);
                    })
            });
        });
    });

});