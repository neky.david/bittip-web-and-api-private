const chai = require('chai');
const chaiHttp = require('chai-http');
const service = require('../model/platforms/stackexchange/stackexchange.service');
const profileService = require('../model/profiles/profiles.service');

const expect = chai.expect;
chai.use(chaiHttp);

var accessToken;

describe('STACKEXCHANGE', () => {

    it('It should get the ACCESS TOKEN', async () => {
        accessToken = (await profileService.findOne(8147682, 'stackoverflow')).accessToken;
        expect(accessToken).to.not.be.null;
    });

    it("Is should get ME by access token", async () => {
        var user = await service.getMe(accessToken);
        expect(user).to.be.a('object');
        expect(user).to.have.property('userId');
        expect(user.userId).to.be.above(0);
    });

    it("Is should get USER by id", async () => {
        var userId = 184046;
        var user = await service.getUserById(userId);
        expect(user).to.be.a('object');
        expect(user).to.have.property('userId');
        expect(user.userId).to.be.eq(userId);
    });

    it("Is should get QUESTION by id", async () => {
        var questionId = 5062614;
        var question = await service.getQuestion(questionId);
        expect(question).to.be.a('object');
        expect(question).to.have.property('questionId');
        expect(question.questionId).to.be.eq(questionId);
    });

    it("Is should get ANSWERS by question", async () => {
        var questionId = 5062614;
        var answers = await service.getAnswersByQuestion(questionId);
        expect(answers).to.be.a('array');
        expect(answers.length).to.be.above(0);
        expect(answers[0].questionId).to.be.eq(questionId);
    });

    it("Is should get all SITES", async () => {
        var logSites = true;
        var sites = await service.getSites();
        expect(sites).to.be.a('array');
        expect(sites.length).to.be.above(0);

        if (logSites) {
            sites.forEach(site => {
                var showStar = !site.site_url.includes('stackexchange') && !site.site_url.includes('stackoverflow.com');
                var star = showStar ? '*' : '';
                console.log(`${star} ${site.name} - ${site.api_site_parameter} - ${site.site_url}`);
            });
        }
    });

});