const Joi = require('joi');
const BaseObject = require('../BaseObject');

class Payment extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.id = this.walletId = this.type = this.amount = this.fees = this.payed = this.payDate = this.payIndex = this.rHash =
                this.payReq = this.memo = this.addIndex = this.payAddress = this.createDate = this.updateDate = undefined;
            Object.assign(this, data);
        }
    }

    isValid() {
        return validSchema.validate(this);
    }

    static columnsToView = 'p.id, p.wallet_id, p.type, p.amount, p.fees, p.payed, p.pay_date, p.pay_req, p.memo';

    toView() {
        return new Payment({
            id: this.id,
            walletId: this.walletId,
            type: this.type,
            amount: this.amount,
            fees: this.fees,
            payed: this.payed,
            payDate: this.payDate,
            payReq: this.payReq,
            platform: this.memo
        })
    }
}

const validSchema = Joi.object({
    id:
        Joi.number().integer().positive().max(99999999999)
            .allow(null).optional(), //allow(null) => null.. optional() => undefined
    walletId:
        Joi.number().integer().positive().max(99999999999)
            .required(),
    type:
        Joi.string()
            .allow(null).optional(),
    amount:
        Joi.number().positive().max(99999999999)
            .required(),
    fees:
        Joi.number().positive().max(99999999999)
            .allow(null).optional(),
    payed:
        Joi.bool()
            .allow(null).optional(),
    payDate:
        Joi.date()
            .allow(null).optional(),
    payIndex:
        Joi.number().integer().positive().max(99999999999)
            .required(),
    rHash:
        Joi.string()
            .required(),
    payReq:
        Joi.string()
            .required(),
    memo:
        Joi.string()
            .allow(null).optional(),
    addIndex:
        Joi.number().integer().positive().max(99999999999)
            .required(),
    payAddress:
        Joi.string()
            .required(),
    createDate:
        Joi.date()
            .allow(null).optional(),
    updateDate:
        Joi.date()
            .allow(null).optional()
});

module.exports = Payment;