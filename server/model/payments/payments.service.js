const Payment = require('./Payment');
const model = require('./payments.model');
const lightningService = require('../../services/lightning/lightning.service');
const lnurlService = require('../../services/lightning/lnurl.service');
const dbService = require('../../services/db.service');
const walletService = require('../wallets/wallets.service');
const UserError = require('../../errors/UserError');
const { HttpStatusCode, ErrorType } = require('../../errors/error.enums');
const PaymentType = require('./PaymentType');

async function findByTipId(tipId, toView) {
    return new Payment(await model.getByTipId(tipId, toView));
};

async function findByHash(rHash, toView) {
    return new Payment(await model.getByHash(rHash, toView));
};

async function findByTypeAndPayReq(type, payReq, toView) {
    return new Payment(await model.getByTypeAndPayReq(type, payReq, toView));
}

async function findOutgoingByLnurlSecret(lnurlSecret, toView) {
    return new Payment(await model.getByTypeAndLnurlSecret(PaymentType.OUTGOING, lnurlSecret, toView));
}

async function findLastByTypeAndWallet(type, walletId, toView) {
    return new Payment(await model.getLastByTypeAndWallet(type, walletId, toView));
}

async function create(payment) {
    return await model.insert(payment, toView);
}

async function createIncomingPayment(walletId, memo, amount = 0, toView) {
    if (!walletId) throw new Error("Wallet Id is required while creating incoming payment");
    var invoice = await lightningService.addInvoice(amount, memo);
    var payment = new Payment();
    payment.walletId = walletId;
    payment.memo = memo;
    payment.type = PaymentType.INCOMING;
    payment.rHash = invoice.rHash;
    payment.payReq = invoice.paymentRequest;
    payment.addIndex = invoice.addIndex;
    payment.payAddress = invoice.paymentAddr;
    return await model.insert(payment, toView);
}

/**
 * 
 * @param {*} walletId 
 * @param {*} amount 
 * @param {*} memo 
 * @param {*} toView 
 * @returns {{ lnurl, Payment }} { lnurl, payment }
 */
async function createOutgoingPayment(walletId, amount, memo, toView) {

    var minimal = parseInt(process.env.WITHDRAW_MINIMAL_MSATS || 100000);

    if (!walletId) throw new Error("Wallet Id is required while creating outgoing payment");
    if (!amount || amount <= minimal) throw new UserError(`Minimum withdraw amount is ${minimal / 1000} sats and you have only ${amount / 1000} sats available.`, HttpStatusCode.FORBIDDEN, "You have too little sats", ErrorType.WARNING);

    var lnurl = await lnurlService.withdraw(amount < minimal ? amount : minimal, amount, memo);
    var payment = new Payment();
    payment.walletId = walletId;
    payment.memo = memo;
    payment.type = PaymentType.OUTGOING;
    payment.lnurlSecret = lnurl.secret;
    // payment.rHash = lnurl.secret;
    // payment.payReq = lnurl.encoded;
    payment = await model.insert(payment, toView);
    return { lnurl, payment };
}

async function waitToPay(paymentHash, toView) {
    if (paymentHash) {
        const invoiceTracker = require('../../services/lightning/invoice.tracker');
        payment = await invoiceTracker.waitToSettle(paymentHash);
        if (toView) payment = payment?.toView();
        return payment;
    }
    else {
        throw new Error(`Tip with id ${tipId} was not found.`)
    }
}

async function update(payment, toView) {
    if (payment.id)
        return await model.update(payment, toView)
}

async function createOrUpdate(payment) {
    if (payment.id)
        return await this.update(payment);
    else
        return await this.create(payment);
}

async function updatePaymentFromSettledInvoice(invoice) { //TODO: change to 'payedIncoming'
    if (invoice) {
        var rHash = invoice.rHash.toString('base64');
        var payment = await findByHash(rHash);
        if (!payment.isEmpty()) {
            dbService.beginTransaction();
            try {
                payment.payed = invoice.settled;
                payment.payDate = invoice.settled ? new Date(invoice.settleDate * 1000) : null;
                payment.amount = parseInt(invoice.amtPaidMsat);
                payment.payIndex = parseInt(invoice.settleIndex);
                payment = await this.update(payment);

                walletService.recalculateBalanceByPayment(payment);

                dbService.commitTransaction();

                return payment;
            }
            catch (err) {
                dbService.rollbackTransaction();
                throw err;
            }
        }
    }
}

/**
 * Calling after outgoin payment was payed - update pay info and recalculate the balance in the wallet
 * @param {Payment} payment
 */
async function payedOutgoing(payment, totalAmount, fees, memo, payDate, payHash, payAddr) {
    if (payment && !payment.isEmpty() && payment.type == PaymentType.OUTGOING) {
        payment.amount = totalAmount;
        payment.fees = fees;
        payment.memo = memo;
        payment.payed = true;
        payment.payDate = payDate ? payDate : new Date();
        payment.payHash = payHash;
        payment.payAddress = payAddr;
        this.update(payment);

        await walletService.recalculateBalanceByPayment(payment);
        require('./withdraw.service').withdrawPayedCallback(payment); //callback to user
    }
}

async function withdraw(user) {
    var wallet = await walletService.findByUserId(user.id);

    if (wallet.isEmpty()) throw new UserError("Cannot withdraw any sats. Sorry.", HttpStatusCode.FORBIDDEN, "Your wallet is empty.", ErrorType.WARNING);

    var { lnurl } = await this.createOutgoingPayment(wallet.id, wallet.availableAmount, "BitTip Withdrawing");
    return lnurl.encoded;
}

var withdrawCallbacks = [];
/**
 * Waiting until the last withdraw payment has been paid 
 * @param {User} user 
 */
async function waitToWithdraw(user){
    var payment
}

/**
 * Find outgoing payment by payReq and check if user has enough sats in wallet 
 * @param {*} payReq 
 * @param {*} toView 
 */
async function findByOutgoingPayReqAndCheckBalance(payReq, toView) {
    if (process.env.TESTING) { console.log('findByOutgoingPayReqAndCheckBalance'); }

    if (!payReq) throw new Error("payReq is required in findByOutgoingPayReqAndCheckBalance");
    var payment = await this.findByTypeAndPayReq(PaymentType.OUTGOING, payReq);
    if (process.env.TESTING) { console.log(payment); }

    if (payment.isEmpty()) throw new Error("payment was not found by payReq");

    var wallet = await walletService.findById(payment.walletId);
    if (process.env.TESTING) { console.log(wallet); }
    if (wallet.isEmpty()) throw new Error("wallet was not found");

    var invoice = await lightningService.checkInvoice(payReq);
    if (process.env.TESTING) { console.log(invoice); }

    //check balance
    if (invoice.numMsat > wallet.availableAmount) throw new Error("There is a lack of sats in the wallet");

    return payment;
}

/**
 * Make copy of payment for each payReq
 * @param {Payment} payment 
 * @param {[String]} payRequests 
 */
async function makeCopyForEachPayRequest(payment, payRequests) {
    if (payRequests && payRequests.length > 0) {
        payRequests.forEach(payReq => {
            payment.payReq = payReq;
            if (process.env.TESTING) {
                console.log('createOrUpdate payment');
                console.log(payment);
            }
            this.createOrUpdate(payment);
            if (payment.id) payment.id = null;
        });
    }
}

module.exports = {
    findByTipId,
    findByHash,
    findByTypeAndPayReq,
    waitToPay,
    create,
    createIncomingPayment,
    createOutgoingPayment,
    updatePaymentFromSettledInvoice,
    update,
    createOrUpdate,
    withdraw,
    findOutgoingByLnurlSecret,
    makeCopyForEachPayRequest,
    findByOutgoingPayReqAndCheckBalance,
    payedOutgoing,
    findLastByTypeAndWallet
}