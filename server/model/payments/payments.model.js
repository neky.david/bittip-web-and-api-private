const db = require('../../services/db.service');
const Payment = require('./Payment');

module.exports = {

    getById: async (paymentId, toView) => {
        let sql = `
            SELECT 
            ${toView ? Payment.columnsToView : 'p.*'} 
            FROM payments WHERE p.id = ?`;
        return await db.query(sql, paymentId, { firstOrNull: true });
    },

    getByTipId: async (tipId, toView) => {
        let sql = `
            SELECT 
            ${toView ? Payment.columnsToView : 'p.*'} 
            FROM payments AS p
            JOIN tips AS t ON p.id = t.payment_id
            WHERE t.id = ?`;
        return await db.query(sql, tipId, { firstOrNull: true });
    },

    getByHash: async (rHash, toView) => {
        let sql = `
            SELECT 
            ${toView ? Payment.columnsToView : 'p.*'} 
            FROM payments AS p
            WHERE p.r_hash = ?`;
        return await db.query(sql, rHash, { firstOrNull: true });
    },

    getByTypeAndPayReq: async (type, payReq, toView) => {
        let sql = `
            SELECT 
            ${toView ? Payment.columnsToView : 'p.*'} 
            FROM payments AS p
            WHERE p.type = ? AND p.pay_req = ?`;
        return await db.query(sql, [type, payReq], { firstOrNull: true });
    },

    getByTypeAndLnurlSecret: async (type, lnurlSecret, toView) => {
        let sql = `
            SELECT 
            ${toView ? Payment.columnsToView : 'p.*'} 
            FROM payments AS p
            WHERE p.type = ? AND p.lnurl_secret = ?`;
        return await db.query(sql, [type, lnurlSecret], { firstOrNull: true });
    },

    getLastByTypeAndWallet: async (type, walletId, toView) => {
        let sql = `
            SELECT 
            ${toView ? Payment.columnsToView : 'p.*'} 
            FROM payments AS p
            WHERE p.type = ? AND p.wallet_id = ?
            ORDER BY p.create_date DESC
            LIMIT 1`;
        return await db.query(sql, [type, walletId], { firstOrNull: true });
    },

    /**
     * 
     * @param {Payment} payment 
     * @param {boolean} toView 
     * @returns 
     */
    insert: async (payment, toView) => {
        let sql = 'INSERT INTO payments SET ?';
        var result = await db.query(sql, payment);
        payment.id = result.insertId;
        if (toView) payment = payment.toView();
        return payment;
    },

    /**
     * 
     * @param {Payment} payment 
     */
    update: async (payment, toView) => {
        let sql = `
            UPDATE payments SET 
                amount = COALESCE(?, amount),
                payed = COALESCE(?, payed),
                pay_date = COALESCE(?, pay_date),
                pay_index = COALESCE(?, pay_index),
                pay_req = COALESCE(?, pay_req),
                memo = COALESCE(?, memo)
            WHERE id = ?`;
        var result = await db.query(sql, [payment.amount, payment.payed, payment.payDate?.toSqlFormat(), payment.payIndex, payment.payReq, payment.memo, payment.id]);
        if (toView) payment = payment.toView();
        return payment;
    },

}