const Payment = require('./Payment');
const paymentService = require('./payments.service');
const walletService = require('../wallets/wallets.service');
const UserError = require('../../errors/UserError');
const { HttpStatusCode, ErrorType } = require('../../errors/error.enums');
const User = require('../users/User');
const PaymentType = require('./PaymentType');

async function withdraw(user) {
    var wallet = await walletService.findByUserId(user.id);

    if (wallet.isEmpty()) throw new UserError("Cannot withdraw any sats. Sorry.", HttpStatusCode.FORBIDDEN, "Your wallet is empty.", ErrorType.WARNING);

    var { lnurl } = await paymentService.createOutgoingPayment(wallet.id, wallet.availableAmount, "BitTip Withdrawing");
    return lnurl.encoded;
}

var withdrawCallbacks = [];

/**
 * Waiting until the last withdraw payment has been paid 
 * @param {User} user 
 * @returns {Payment} payment
 */
async function waitToWithdraw(user, toView) {
    return new Promise((resolve, reject) => {
        if (!user) reject(new Error("waitToWithdraw - User is required"));

        paymentService.findLastByTypeAndWallet(PaymentType.OUTGOING, user.walletId)
        .then(payment => {
            if (process.env.TESTING) {
                console.log('waitToWithdraw - payment:');
                console.log(payment);
            }
    
            if (!payment.isEmpty()) {
                addWithdrawCallback(payment.id, toView, resolve, reject);
            }
        })
        .catch(err => {
            reject(err);
        })
    });
}

async function withdrawPayedCallback(payment) {
    //call waiting func
    var item = withdrawCallbacks.find(c => c.paymentId === payment.id);
    
    if (process.env.TESTING) {
        console.log('withdrawPayedCallback - callback');
        console.log(item);
    }

    if (item && item.onSuccess) {
        try {
            if(item.toView) payment = payment.toView();
            item.onSuccess(payment);
        }
        catch (err) {
            console.log(err);
        }
        removeWithdrawCallback(item.paymentId);
    }
}

function addWithdrawCallback(paymentId, toView, onSuccess, onError) {
    withdrawCallbacks.push({
        paymentId: paymentId,
        toView: toView,
        onSuccess: onSuccess,
        onError: onError,
        addDate: new Date()
    })
    if (process.env.TESTING) {
        console.log('withdraw callbacks:');
        console.log(withdrawCallbacks);
    }
}

function removeWithdrawCallback(paymentId) {
    var hourBefore = new Date().addHours(-1);
    withdrawCallbacks = withdrawCallbacks.filter(c => c.paymentId != paymentId && c.addDate > hourBefore);
}

module.exports = {
    withdraw,
    waitToWithdraw,
    withdrawPayedCallback
}