const axios = require('axios').default;
const { parse } = require('node-html-parser');
const { MediumAccount, MediumPublication, MediumContributor, MediumPost } = require('./medium.classes');

/**
 * 
 * @param {string} integrationToken 
 * @returns {MediumAccount}
 */
async function getAccount(integrationToken) {
    if (!integrationToken) throw new Error("integrationToken is required!");
    var account = new MediumAccount();

    var response = await axios.get('https://api.medium.com/v1/me', {
        headers: {
            'Authorization': 'Bearer ' + integrationToken
        }
    });
    if (response.status == 200)
        return new MediumAccount(response.data.data);

    return account;
}

/**
 * 
 * @param {string} playlistId 
 * @param {string} accessToken 
 * @param {string} pageToken 
 * @returns {MediumPublication[]}
 */
async function getPublications(userId, integrationToken) {
    if (!userId) throw new Error("userId is required!");
    if (!integrationToken) throw new Error("integrationToken is required!");

    var publications = [];

    var response = await axios.get(`https://api.medium.com/v1/users/${userId}/publications`, {
        headers: {
            'Authorization': 'Bearer ' + integrationToken
        }
    })
    if (response.status == 200)
        publications = MediumPublication.listFromData(response.data);

    return publications;
}

/**
 * 
 * @param {string} playlistId 
 * @param {string} accessToken 
 * @param {string} pageToken 
 * @returns {MediumContributor[]}
 */
async function getContributorsByPublicationId(publicationId, integrationToken) {
    if (!publicationId) throw new Error("publicationId is required!");
    if (!integrationToken) throw new Error("integrationToken is required!");

    var contributors = [];

    var response = await axios.get(`https://api.medium.com/v1/publications/${publicationId}/contributors`, {
        headers: {
            'Authorization': 'Bearer ' + integrationToken
        }
    })
    if (response.status == 200)
        contributors = MediumContributor.listFromData(response.data);

    return contributors;
}

async function getPostDetailFromUrl(url) {
    if (!url) throw new Error("url is required!");

    var response = await axios.get(url);

    const root = parse(response.data);

    var post = new MediumPost();

    //check if it's MEDIUM page
    if (root.querySelector("meta[property='og:site_name']")?.attrs['content'] == 'Medium') {
        post.jsonld = root.querySelector("script[type='application/ld+json']")?.innerText;

        if (post.jsonld) {
            var jsonLd = JSON.parse(post.jsonld);
            post.url = jsonLd.url;
            post.id = jsonLd.identifier;
            post.authorUrl = jsonLd.author.url;
            post.authorName = jsonLd.author.name;
            post.title = jsonLd.name;
            post.description = jsonLd.description;
            post.publishDate = jsonLd.datePublished;
        }
    }

    return post;
}

module.exports = {
    getAccount,
    getPublications,
    getContributorsByPublicationId,
    getPostDetailFromUrl
}