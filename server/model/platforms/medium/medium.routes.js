const router = require('express').Router();
const mdService = require('./medium.service');
const {auth} = require('../../auth/auth.jwt');
const UserError = require('../../../errors/UserError');
const { HttpStatusCode } = require('../../../errors/error.enums');
const asyncHandler = require('express-async-handler')

router.post('/connect', auth, asyncHandler(async(req, res, next) => {

    if(req.body && req.body.token){
        var profile = await mdService.createOrUpdateProfileFromToken(req.body.token, req.user.id, true);
        res.send(profile);
    }
    else{
        next(new UserError('Integration token is required!', HttpStatusCode.BAD_REQUEST));
    }

}));

module.exports = router;