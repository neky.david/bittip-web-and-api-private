const { PLATFORMS } = require("../../enums");
const BaseObject = require("../../BaseObject");
const Content = require("../../contents/Content");
const Profile = require("../../profiles/Profile");

class MediumPost extends BaseObject {
    constructor() {
        super(); //call ancestor constructor
        this.id = this.url = this.authorName = this.authorUrl = this.title = this.description = this.jsonld = this.publishDate = null;
    }

    toContent() {
        var content = new Content();
        content.privateId = this.id;
        content.publicId = this.url;
        content.platform = PLATFORMS.MEDIUM;
        content.masterId = this.authorUrl;
        content.url = this.url;
        content.title = this.title;
        content.description = this.description;
        content.publishDate = new Date(this.publishDate);
        return content;
    }
}

class MediumAccount extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.id = data.id;
            this.username = data.username;
            this.name = data.name;
            this.url = data.url;
            this.imageUrl = data.imageUrl;
        }
    }

    /**
     * 
     * @param {number} userId 
     * @returns {Profile}
     */
    toProfile(userId){
        var profile = new Profile();
        profile.platformUserId = this.id;
        profile.contentMasterId = this.url; //this.username
        profile.userName = this.username;
        profile.displayName = this.name;
        profile.fullName = this.name;
        profile.userId = userId;
        profile.platform = PLATFORMS.MEDIUM;
        profile.url = this.url;
        profile.imgUrl = this.imageUrl;
        return profile;
    }

}

class MediumPublication extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.id = data.id;
            this.name = data.name;
            this.description = data.description;
            this.url = data.url;
            this.imageUrl = data.imageUrl;
        }
    }

    static listFromData(listOfData) {
        var publications = [];

        if (!Array.isArray(listOfData) && listOfData.hasOwnProperty("data"))
            listOfData = listOfData.data;

        if (Array.isArray(listOfData)) {
            listOfData.forEach(publicationData => {
                publications.push(new MediumPublication(publicationData));
            });
        }

        return publications;
    }

}

class MediumContributor extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.publicationId = data.publicationId;
            this.userId = data.userId;
            this.role = data.role;
        }
    }

    static listFromData(listOfData) {
        var contributors = [];

        if (!Array.isArray(listOfData) && listOfData.hasOwnProperty("data"))
            listOfData = listOfData.data;

        if (Array.isArray(listOfData)) {
            listOfData.forEach(contributorData => {
                contributors.push(new MediumContributor(contributorData));
            });
        }
        return contributors;
    }

}

module.exports = {
    MediumPost,
    MediumAccount,
    MediumPublication,
    MediumContributor
}