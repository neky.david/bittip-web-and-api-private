const access = require('./medium.access');
const { MediumPost, MediumAccount } = require('./medium.classes');
const profilesService = require('../../profiles/profiles.service');
const UserError = require('../../../errors/UserError');
const Content = require('../../contents/Content');
const { PLATFORMS } = require('../../enums');
const { HttpStatusCode, ErrorType } = require('../../../errors/error.enums');

/**
 * 
 * @param {MediumAccount} integrationToken 
 * @returns 
 */
async function getAccount(integrationToken) {
    return await access.getAccount(integrationToken);
}

async function getPublications(userId, integrationToken) {
    return await access.getPublications(userId, integrationToken);
}

async function getContributorsByPublicationId(publicationId, integrationToken) {
    return await access.getContributorsByPublicationId(publicationId, integrationToken);
}

/**
 * 
 * @param {string} url post url
 * @returns {MediumPost}
 */
async function getPostDetailFromUrl(url) {
    return await access.getPostDetailFromUrl(url);
}

async function createOrUpdateProfileFromToken(integrationToken, userId, toView) {
    var account = await getAccount(integrationToken);
    var platformProfile = account.toProfile();
    var dbProfile;

    var oldProfile = await profilesService.findByPlatformUser(platformProfile.platformUserId, PLATFORMS.MEDIUM, false, false)
    //if (!(await profilesService.exist(platformProfile))) 
    if (oldProfile.isEmpty()) {
        //PROFILE NOT EXIST
        platformProfile.userId = userId;
        dbProfile = await profilesService.create(platformProfile, toView);
    }
    else {
        if (!oldProfile.active || oldProfile.userId == userId) {
            platformProfile.id = oldProfile.id;
            platformProfile.active = true;
            dbProfile = await profilesService.update(platformProfile, toView);
        }
        else {
            //cannot connect because is connected to other account
            throw new UserError('This Medium account is already connected to another BitTip account!', HttpStatusCode.BAD_REQUEST, 'Account already connected', ErrorType.WARNING);
        }
    }
    return dbProfile;
}

async function findContent(urlId) {
    var content = new Content();
    var post = await getPostDetailFromUrl(urlId);
    if (!post.isEmpty()) {
        content = post.toContent();
    }
    return content;
}

module.exports = {
    getAccount,
    getPublications,
    getContributorsByPublicationId,
    getPostDetailFromUrl,
    createOrUpdateProfileFromToken,
    findContent
}