const { PLATFORMS } = require("../../enums");
const BaseObject = require("../../BaseObject");
const Content = require("../../contents/Content");
const Profile = require("../../profiles/Profile");

const POST_TYPE = Object.freeze({ 'ANSWER':'answer', 'QUESTION':'question'});

class StackUser extends BaseObject {
    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.userId = data.user_id;
            this.accountId = data.account_id;
            this.creationDate = data.creation_date ? new Date(data.creation_date) : undefined;
            this.userType = data.user_type;
            this.link = data.link;
            this.profileImage = data.profile_image;
            this.displayName = data.display_name;
        }
    }

    /**
     * 
     * @returns {Profile}
     */
    toProfile() {
        var profile = new Profile();
        profile.platformUserId = this.userId.toString();
        profile.contentMasterId = this.userId.toString();
        profile.displayName = this.displayName;
        profile.platform = PLATFORMS.STACKEXCHANGE;
        profile.url = this.link;
        profile.imgUrl = this.profileImage;
        return profile;
    }
}

class StackPost extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.postId = data.answer_id || data.question_id || data.post_id;
            this.questionId = data.question_id;
            this.answerId = data.answer_id;
            this.postType = data.post_type;
            this.creationDate = data.creation_date ? new Date(data.creation_date) : undefined;
            this.link = data.link;
            this.score = data.score;
            this.title = data.title; //only question
            this.description = data.body;
            this.userId = data.owner?.user_id;
            this.userDisplayName = data.owner?.display_name;
            this.userRegistred = data.owner?.registred === 'registered';
        }
    }

    toContent() {
        var content = new Content();
        content.privateId = this.postId;
        content.publicId = this.postId;
        content.masterId = this.userId;
        content.platform = PLATFORMS.STACKEXCHANGE;
        content.url = this.link;
        content.title = this.title;
        content.description = this.description.htmlToPlain();
        content.publishDate = this.creationDate;
        return content;
    }

    static listFromData(data) {
        var posts = [];

        if (!Array.isArray(data) && data.hasOwnProperty("items"))
            data = data.items;

        if (Array.isArray(data)) {
            data.forEach(postData => {
                posts.push(new StackPost(postData));
            });
        }

        return posts;
    }
}

class StackComment extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.commentId = data.comment_id;
            this.postId = data.post_id;
            this.creationDate = data.creation_date ? new Date(data.creation_date) : undefined;
            this.score = data.score;
            this.body = data.body;
            this.userId = data.owner?.user_id;
            this.userDisplayName = data.owner?.display_name;
            this.userRegistred = data.owner?.registred === 'registered';
        }
    }

    toContent() {
        var content = new Content();
        content.privateId = this.commentId;
        content.publicId = this.commentId;
        content.masterId = this.userId;
        content.platform = PLATFORMS.STACKEXCHANGE;
        content.description = this.body.htmlToPlain();
        content.publishDate = this.creationDate;
        return content;
    }
}


module.exports = {
    StackUser,
    StackPost,
    StackComment,
    POST_TYPE
}