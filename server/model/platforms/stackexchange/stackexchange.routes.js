const router = require('express').Router();
const stackAuth = require('./stackexchange.auth');
const { auth, jwtFromUser } = require('../../auth/auth.jwt');

router.get('/auth', (req, res) => {

    var redirectTo = req.query.redirect_to;

    stackAuth({
        session: false,
        state: JSON.stringify({ redirect_to: redirectTo })
    })(req, res);
});

router.get('/connect', auth, (req, res) => {

    var redirectTo = req.query.redirect_to;

    stackAuth({
        session: false,
        state: JSON.stringify({
            redirect_to: redirectTo,
            userId: req.user.id,
            connect: true
        })
    })(req, res);
});

router.get('/callback', (req, res, next) => {
    var state = JSON.parse(req.query.state);
    
    if (process.env.TESTING) {
        console.log('callback state');
        console.log(state);
    }

    return stackAuth({
        session: false,
        failureRedirect: state.redirect_to
    })(req, res, next);
},
    (req, res) => {
        if (req.user) {
            var jwtToken = jwtFromUser(req.user);
            res.cookie('jwt', jwtToken, {
                maxAge: 86400000,
                httpOnly: true,
                secure: process.env.NODE_ENV === 'production' ? true : false
            });
        }

        var state = JSON.parse(req.query.state);
        if (state.redirect_to)
            res.redirect(state.redirect_to);
    });


module.exports = router;