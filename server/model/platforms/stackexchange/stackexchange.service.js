const Profile = require('../../profiles/Profile');
const stackAccess = require('./stackexchange.access');
const contentService = require('../../contents/contents.service');
const { StackUser } = require('./stackexchange.classes');
const profileService = require('../../profiles/profiles.service');
const Content = require('../../contents/Content');

/**
 * Get USER INFO by access token
 * 
 * @param {string} accessToken 
 * @returns {StackUser}
 */
async function getMe(accessToken, site) {
    return await stackAccess.getMe(accessToken, site);
}

/**
 * Get USER INFO by id
 * 
 * @param {string} userId 
 * @returns {StackUser}
 */
async function getUserById(userId, site) {
    return await stackAccess.getUserById(userId, site);
}

/**
 * Get POSTS (questions and answers) owned by user
 * 
 * @param {string} accessToken 
 * @returns {StackPost[]}
 */
async function getMyPosts(accessToken, page, site) {
    return await stackAccess.getMyPosts(accessToken, page, site)
}

/**
 * Save all user's posts to db as content records
 *
 * @param {Profile} profile 
 * @returns {string} error message
 */
async function syncMyPosts(profile, site) {
    var message = null;
    var posts = [];
    posts.hasMore = true;

    if (!profile.contentSyncDate) {
        if (!profile.accessToken || profile.expireDate <= Date.now())
            message = 'accessToken is not valid or expired';
        else {
            try {
                var page = 1;
                //sync every page
                while (posts.hasMore) {
                    posts = await this.getMyPosts(profile.accessToken, page, site)

                    posts.forEach(post => {
                        var content = post.toContent();
                        if (!content.isEmpty())
                            contentService.create(content);
                    });
                    page++;
                }
            } catch (err) {
                message = err.message;
            }

            //update profile
            profile.contentSyncDate = new Date(Date.now());
            profileService.update(profile);
        }
    }

    return message;
}

/**
 * Get QUESTION DETAIL by questionId
 *
 * @param {number} questionId
 * @returns {StackPost} question
 */
async function getQuestion(questionId, site) {
    return await stackAccess.getQuestion(questionId, site);
}

/**
 * Get all ANSWERS related to question
 *
 * @param {number} questionId
 * @returns {StackPost[]} answers
 */
async function getAnswersByQuestion(questionId, site) {
    var answers = [];

    if (questionId) {
        var hasMore = true;
        var page = 1;

        while (hasMore == true) {
            var { answers: nextAnswers, hasMore } = await stackAccess.getAnswersByQuestion(questionId, page, site);
            answers = answers.concat(nextAnswers);
            page++;
        }
    }
    return answers;
}

/**
 * Get list of all SITES in stackexchange
 */
async function getSites() {
    return await stackAccess.getSites();
}

async function getPostById(postId, site) {
    return await stackAccess.getPostById(postId, site);
}

async function getCommentById(commnetId, site) {
    return await stackAccess.getCommentById(commnetId, site);
}

/**
 * 
 * @param {Number} contentId 
 * @param {string} type (post/comment)
 */
async function findContent(contentId, type, site) {
    var content = new Content();
    if (type === "comment") {
        var comment = await getCommentById(contentId, site);
        if (!comment.isEmpty()) content = comment.toContent();
    }
    else {
        var post = await getPostById(contentId, site);
        if (!post.isEmpty()) content = post.toContent();
    }
    return content;
}

module.exports = {
    getMe,
    getUserById,
    getMyPosts,
    syncMyPosts,
    getQuestion,
    getAnswersByQuestion,
    getSites,
    getCommentById,
    findContent
}