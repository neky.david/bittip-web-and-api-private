const passport = require('passport');
const OAuth2Strategy = require('passport-oauth2');
const { PLATFORMS } = require('../../enums');
const Profile = require('../../profiles/Profile');
const stackService = require('./stackexchange.service');
const userService = require('../../users/users.service');
const profilesService = require('../../profiles/profiles.service');
const authJwt = require('../../auth/auth.jwt');
const moment = require('moment');
const UserError = require('../../../errors/UserError')

passport.use("stackexchange", new OAuth2Strategy({
    authorizationURL: 'https://stackoverflow.com/oauth',
    tokenURL: 'https://stackoverflow.com/oauth/access_token/json',
    clientID: process.env.STACKEXCHANGE_CLIENT_ID,
    clientSecret: process.env.STACKEXCHANGE_CLIENT_SECRET,
    callbackURL: process.env.API_URL + "/stackexchange/callback",
    passReqToCallback: true
},
    async function (req, accessToken, _/*refreshToken*/, params, _/*profile*/, cb) {
    
        console.log("stackexchange auth");

        if (params.error_message)
            cb(new UserError(params.error_message), null);

        var dbProfile, dbUser;

        try {
            var state = JSON.parse(req.query.state);
            if (state.connect && state.userId) {
                //CONNECT
                var dbUser = await userService.findById(state.userId);
                if (!dbUser.isEmpty()) {

                    var platformProfile = await getProfileFromAccessToken(accessToken, params.expires);
                    platformProfile.userId = dbUser.id;

                    var oldProfile = await profilesService.findByPlatformUser(platformProfile.platformUserId, PLATFORMS.STACKEXCHANGE, false, false);
                    //if (!(await profilesService.exist(platformProfile))) 
                    if(oldProfile.isEmpty()){
                        //PROFILE NOT EXIST - save new to db
                        dbProfile = await profilesService.create(platformProfile);
                    }
                    else{
                        //PROFILE EXIST - update it
                        if (!oldProfile.active || oldProfile.userId == dbUser.id) {
                            platformProfile.id = oldProfile.id;
                            platformProfile.active = true;
                            dbProfile = await profilesService.update(platformProfile);
                        }
                        else{
                            //cannot connect because is connected to other account
                            cb(new UserError('This StackExchange account is already connected to another BitTip account!'));
                        }
                    }
                }
            }
            else {
                //NEW USER
                var platformProfile = await getProfileFromAccessToken(accessToken, params.expires);

                var { user: dbUser, profile: dbProfile } = await userService.findOrCreateByPlatformProfile(platformProfile);

                //post SYNCHRONIZATION
                /*stackService.syncMyPosts(profile).then(err => {
                    if (err)
                        console.log('error while post synchonization - ' + err);
                    else
                        console.log("posts was successfully synchronized");
                });*/
            }

            if (dbUser)
                cb(null, dbUser);
            else
                cb(null, false);
        }
        catch (err) {
            cb(err, null);
        }
    }
));

/**
 * 
 * @param {String} accessToken 
 * @param {*} expires 
 * @returns {Profile}
 */
async function getProfileFromAccessToken(accessToken, expires) {
    var stackUser = await stackService.getMe(accessToken, 'stackoverflow'); //TODO: remove stackoverflow

    var expireDate = moment().add(expires, 's');

    //create profile
    var profile = stackUser.toProfile();
    profile.accessToken = accessToken;
    profile.expireDate = expireDate.toDate();

    //validate profile
    var validResult = profile.isValid();
    if (validResult.error) throw new Error(validResult.error);

    return profile;
}

module.exports = (options) => {
    return passport.authenticate('stackexchange', options);
}