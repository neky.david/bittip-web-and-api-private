const axios = require('axios').default;
const { StackUser, StackPost, StackComment } = require('./stackexchange.classes');

/**
 * 
 * @param {string} accessToken 
 * @returns {StackUser}
 */
async function getMe(accessToken, site) {
    if (!accessToken) throw new Error("accessToken is required!");
    if (!site) throw new Error("site is required!");
    var user = new StackUser();

    try {
        var response = await axios.get('https://api.stackexchange.com/2.2/me', {
            params: {
                site: site,
                access_token: accessToken,
                key: process.env.STACKEXCHANGE_API_KEY
            }
        });
        if (response.status == 200 && response.data.items?.length > 0)
            user = new StackUser(response.data.items[0]);
    } catch (error) {
        throw new Error(`Error while accessing stackexchange API (/me): ${error.message} - ${error.response.data.error_message}`);
    }

    return user;
}

/**
 * 
 * @param {string} accessToken 
 * @returns {StackUser}
 */
async function getUserById(userId, site) {
    if (!userId) throw new Error("userId is required!");
    if (!site) throw new Error("site is required!");
    var user = new StackUser();

    try {
        var response = await axios.get(`https://api.stackexchange.com/2.2/users/${userId}`, {
            params: {
                site: site,
                key: process.env.STACKEXCHANGE_API_KEY
            }
        });
        if (response.status == 200 && response.data.items?.length > 0)
            user = new StackUser(response.data.items[0]);
    } catch (error) {
        throw new Error(`Error while accessing stackexchange API (/users): ${error.message} - ${error.response.data.error_message}`);
    }

    return user;
}

/**
 * 
 * @param {string} accessToken 
 * @returns {StackPost[]}
 */
async function getMyPosts(accessToken, page, site) {
    if (!accessToken) throw new Error("accessToken is required!");
    if (!site) throw new Error("site is required!");
    var posts = [];

    try {
        var response = await axios.get('https://api.stackexchange.com/2.2/me/posts', {
            params: {
                site: site,
                access_token: accessToken,
                key: process.env.STACKEXCHANGE_API_KEY,
                pagesize: 100, //max 100
                page: page
            }
        });
        if (response.status == 200 && response.data.items?.length > 0) {
            posts = StackPost.listFromData(response.data);
            posts.hasMore = response.data.has_more;
        }
    } catch (error) {
        throw new Error(`Error while accessing stackexchange API (/me/posts): ${error.response.data.error_name} - ${error.response.data.error_message}`);
    }

    return posts;
}

/**
 * 
 * @param {number} questionId 
 * @returns {StackPost}
 */
async function getQuestionById(questionId, site) {
    if (!questionId) throw new Error("questionId is required!");
    if (!site) throw new Error("site is required!");
    var question = new StackPost();

    try {
        var response = await axios.get(`https://api.stackexchange.com/2.2/questions/${questionId}`, {
            params: {
                site: site,
                key: process.env.STACKEXCHANGE_API_KEY
            }
        });
        if (response.status == 200 && response.data.items?.length > 0) {
            question = new StackPost(response.data.items[0])
        }
    } catch (error) {
        throw new Error(`Error while accessing stackexchange API (/questions): ${error.response.data.error_name} - ${error.response.data.error_message}`);
    }

    return question;
}

/**
 * 
 * @param {string} accessToken 
 * @returns {{StackPost[] boolean}} {answers hasMore}
 */
async function getAnswersByQuestion(questionId, page, site) {
    if (!questionId) throw new Error("questionId is required!");
    if (!site) throw new Error("site is required!");
    var answers = [];
    var hasMore = false;

    try {
        var response = await axios.get(`https://api.stackexchange.com/2.2/questions/${questionId}/answers`, {
            params: {
                site: site,
                key: process.env.STACKEXCHANGE_API_KEY,
                sort: 'votes',
                order: 'desc',
                pagesize: 100, //max 100
                page: page
            }
        });
    } catch (error) {
        throw new Error(`Error while accessing stackexchange API (/questions/answers): ${error.response.data.error_name} - ${error.response.data.error_message}`);
    }

    if (response.status == 200 && response.data.items?.length > 0) {
        answers = StackPost.listFromData(response.data);
        hasMore = response.data.has_more;
    }

    return { answers, hasMore };
}

/**
 * 
 * @returns {Array} 
 */
async function getSites() {
    var sites = [];

    try {
        var response = await axios.get('https://api.stackexchange.com/2.2/sites', {
            params: {
                key: process.env.STACKEXCHANGE_API_KEY,
                pagesize: 1000,
            }
        });
        if (response.status == 200 && response.data.items?.length > 0) {
            sites = response.data.items;
        }
    } catch (error) {
        throw new Error(`Error while accessing stackexchange API (/sites): ${error.response.data.error_name} - ${error.response.data.error_message}`);
    }

    return sites;
}

/**
 * 
 * @param {number} commnetId 
 * @returns {StackComment}
 */
async function getCommentById(commnetId, site) {
    if (!commnetId) throw new Error("commnetId is required!");
    if (!site) throw new Error("site is required!");
    var commnet = new StackComment();

    try {
        var response = await axios.get(`https://api.stackexchange.com/2.3/comments/${commnetId}`, {
            params: {
                site: site,
                key: process.env.STACKEXCHANGE_API_KEY,
                filter: 'withbody'
            }
        });
        if (response.status == 200 && response.data.items?.length > 0) {
            commnet = new StackComment(response.data.items[0])
        }
    } catch (error) {
        throw new Error(`Error while accessing stackexchange API (/comments): ${error.response.data.error_name} - ${error.response.data.error_message}`);
    }

    return commnet;
}

/**
 * 
 * @param {number} postId 
 * @returns {StackPost}
 */
async function getPostById(postId, site) {
    if (!postId) throw new Error("postId is required!");
    if (!site) throw new Error("site is required!");
    var post = new StackPost();

    try {
        var response = await axios.get(`https://api.stackexchange.com/2.3/posts/${postId}`, {
            params: {
                site: site,
                key: process.env.STACKEXCHANGE_API_KEY,
                filter: 'withbody'
            }
        });
        if (response.status == 200 && response.data.items?.length > 0) {
            post = new StackPost(response.data.items[0])
        }
    } catch (error) {
        throw new Error(`Error while accessing stackexchange API (/posts): ${error.response.data.error_name} - ${error.response.data.error_message}`);
    }

    return post;
}

//https://api.stackexchange.com/2.2/sites?page=1&pagesize=1000

module.exports = {
    getMe,
    getUserById,
    getMyPosts,
    getQuestionById,
    getCommentById,
    getPostById,
    getAnswersByQuestion,
    getSites
}