const axios = require('axios').default;
const { Video, Channel } = require('./youtube.classes');

const apiKey = process.env.GOOGLE_API_KEY;

/**
 * Return list of user's channels
 * 
 * @param {string} accessToken 
 * @returns {Channel[]}
 */
async function getChannels(accessToken) {
    if (!accessToken) throw new Error("accessToken is required!");
    var response = await axios.get('https://www.googleapis.com/youtube/v3/channels', {
        headers: {
            'Authorization': 'Bearer ' + accessToken
        },
        params: {
            part: 'id,snippet,contentDetails',
            maxResults: 1,
            mine: true
        }
    });
    if (response.status == 200)
        return Channel.listFromData(response.data);

    return [];
}

/**
 * 
 * @param {string} playlistId 
 * @param {string} accessToken 
 * @param {string} pageToken 
 * @returns {{Video[], string}}
 */
async function getVideosByPlaylistId(playlistId, accessToken, pageToken) {
    if (!playlistId) throw new Error("playlistId is required!");

    var videos = [];
    var nextPageToken = null;

    var response = await axios.get('https://youtube.googleapis.com/youtube/v3/playlistItems', {
        headers: {
            'Authorization': 'Bearer ' + accessToken
        },
        params: {
            part: 'id,snippet',
            playlistId: playlistId,
            pageToken: pageToken,
            maxResults: 1, //50, TODO: remove 1
            key: apiKey
        }
    })
    if (response.status == 200){
        videos = Video.listFromData(response.data);
        nextPageToken = response.data.nextPageToken;
    }

    return {videos, nextPageToken};
}

/**
 * 
 * @param {string} id 
 * @returns {Video} one video
 */
async function getVideoById(id) {
    if (!id) throw new Error("video id is required!");

    var video = new Video();

    var response = await axios.get('https://www.googleapis.com/youtube/v3/videos', {
        params: {
            part: 'id,snippet',
            id: id,
            key: apiKey
        }
    })
    if (response.status == 200) {
        var videos = Video.listFromData(response.data);
        if (videos.length > 0) video = videos[0];
    }

    return video;
}

/**
 * 
 * @param {*} refreshToken 
 * @returns {[string, Date]} returns [accessToken, expireDate]
 */
async function refreshAccessToken(refreshToken) {
    if (!refreshToken) throw new Error('refreshToken is required!')

    var accessToken = expireDate = null;

    var response = await axios.post('https://oauth2.googleapis.com/token', {
        client_id: process.env.GOOGLE_CLIENT_ID,
        client_secret: process.env.GOOGLE_CLEINT_SECRET,
        grant_type: 'refresh_token',
        refresh_token: refreshToken
    })

    if (response.status == 200) {
        if (response.data.access_token)
            accessToken = response.data.access_token;
        if (response.data.expires_in){
            expireDate = new Date(Date.now());
            expireDate.setSeconds(expireDate.getSeconds() + response.data.expires_in);
        }
    }

    return [accessToken, expireDate];
}

module.exports = {
    getChannels,
    getVideosByPlaylistId,
    getVideoById,
    refreshAccessToken
}