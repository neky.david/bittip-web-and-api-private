const Profile = require('../../profiles/Profile');
const ytAccess = require('./youtube.access');
const { Video, Channel } = require('./youtube.classes');
const profileService = require('../../profiles/profiles.service');
const Content = require('../../contents/Content');
const contentService = require('../../contents/contents.service');

/**
 * Return list of user's channels
 * 
 * @param {string} accessToken 
 * @returns {Channel[]} list of user's channels
 */
async function getChannels(accessToken) {
    return await ytAccess.getChannels(accessToken);
}

/**
 * Return list of user's channels
 * 
 * @param {string} accessToken 
 * @returns {Channel} list of user's channels
 */
async function getFirstChannel(accessToken) {
    var channel = new Channel();
    var channels = await ytAccess.getChannels(accessToken);
    if (channels.length > 0) channel = channels[0];
    return channel;
}

/**
 * 
 * @param {string} playlistId 
 * @param {string} accessToken
 * @returns {Video[]} list of videos in playlist
 */
async function getVideosByPlaylistId(playlistId, accessToken) {
    var { videos, nextPageToken } = await ytAccess.getVideosByPlaylistId(playlistId, accessToken);
    while (nextPageToken){
        var {videos: nextPageVideos, nextPageToken} = await ytAccess.getVideosByPlaylistId(playlistId, accessToken, nextPageToken);
        videos = videos.concat(nextPageVideos);
    }
    return videos;
}

/**
 * 
 * @param {string} id 
 * @returns {Video} one video
 */
async function getVideoById(id) {
    return await ytAccess.getVideoById(id);
}

/**
 * find video by Id and return it as Content object
 * @param {Number} videoId 
 * @returns 
 */
async function findContent(videoId){
    var content = new Content();
    var video = await getVideoById(videoId);
    if(!video.isEmpty()) content = video.toContent();
    return content;
}

async function refreshAccessToken(refreshToken) {
    return await ytAccess.refreshAccessToken(refreshToken);
}

/**
 * get all user's youtube videos and saves them to database as content
 * 
 * @param {Profile} profile 
 * @param {string} accessToken 
 */
async function syncVideos(profile) {
    var channel = new Channel();
    var updateProfile = false;

    //check if contentMasterId conatins channel id (contentMasterId is defult set to platformUserId)
    if (profile.contentMasterId == profile.platformUserId) {
        channel = await this.getFirstChannel(profile.accessToken);
        if (!channel.isEmpty() && channel.id) {
            profile.contentMasterId = channel.id;
            updateProfile = true;
        }
    }

    //check playlistId
    var uploadsPlaylistId = null;
    var uploadsPlaylistIdParam = await profile.getExtParam("uploadsPlaylistId");
    if (!uploadsPlaylistIdParam.isEmpty())
        uploadsPlaylistId = uploadsPlaylistIdParam.value;
    else {
        if (channel.isEmpty())
            channel = await this.getFirstChannel(profile.accessToken);

        if (!channel.isEmpty()) {
            uploadsPlaylistId = channel.uploadsPaylistId;
            profile.setExtParam("uploadsPlaylistId", uploadsPlaylistId);
        }
    }

    //videos not sync
    if (uploadsPlaylistId && !profile.contentSyncDate) {
        var videos = await this.getVideosByPlaylistId(uploadsPlaylistId, profile.accessToken);

        videos.forEach(video => {
            var content = video.toContent(profile.id);
            contentService.create(content);
        });

        profile.contentSyncDate = new Date(Date.now());
        updateProfile = true;
    }

    //update profile
    if(updateProfile) profileService.update(profile);
}

module.exports = {
    getChannels,
    getFirstChannel,
    getVideosByPlaylistId,
    getVideoById,
    refreshAccessToken,
    syncVideos,
    findContent
}