const router = require('express').Router();
const ytAuth = require('./youtube.auth');
const { auth, jwtFromUser } = require('../../auth/auth.jwt')

//router.get('/google', passport.authenticate('google', { session: false, scope: ['https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email'] }));

router.get('/auth', (req, res) => {

    var redirectTo = req.query.redirect_to;

    ytAuth({
        session: false,
        scope: ['https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/youtube.readonly'
        ],
        accessType: 'offline',
        state: JSON.stringify({ redirect_to: redirectTo })

        //, prompt: 'select_account' //force user to choose account (even if he is logged)
    })(req, res)
});

router.get('/connect', auth, (req, res) => {

    var redirectTo = req.query.redirect_to;

    ytAuth({
        session: false,
        scope: ['https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/youtube.readonly'
        ],
        accessType: 'offline',
        state: JSON.stringify({
            redirect_to: redirectTo,
            userId: req.user.id,
            connect: true
        }),
        prompt: 'select_account' //force user to choose account (even if he is logged)
    })(req, res)
});

router.get('/callback', (req, res, next) => {
    var state = JSON.parse(req.query.state);

    return ytAuth({
        session: false,
        failureRedirect: state.redirect_to
    })(req, res, next);
},
    (req, res) => {
        if (req.user) {
            var jwtToken = jwtFromUser(req.user);
            res.cookie('jwt', jwtToken, {
                maxAge: 86400000,
                httpOnly: true,
                secure: process.env.NODE_ENV === 'production' ? true : false
            });
        }

        var state = JSON.parse(req.query.state);
        if (state.redirect_to)
            res.redirect(state.redirect_to);
    });

module.exports = router;