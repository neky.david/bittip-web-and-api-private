const passport = require('passport');
var GoogleStrategy = require('passport-google-oauth20').Strategy;
const userService = require('../../users/users.service');
const profilesService = require('../../profiles/profiles.service');
const authJwt = require('../../auth/auth.jwt');
const Profile = require('../../profiles/Profile');
const ytService = require('./youtube.service');
const { PLATFORMS } = require('../../enums');
const moment = require('moment');
const UserError = require('../../../errors/UserError');


passport.use(new GoogleStrategy({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLEINT_SECRET,
    callbackURL: process.env.API_URL + "/youtube/callback",
    passReqToCallback: true
},
    async (req, accessToken, refreshToken, params, googleProfile, done) => {

        var dbProfile, dbUser;

        try {
            var state = JSON.parse(req.query.state);
            if (state.connect && state.userId) {
                //CONNECT
                var dbUser = await userService.findById(state.userId);
                if (!dbUser.isEmpty()) {

                    var platformProfile = getProfile(googleProfile, accessToken, refreshToken, params);
                    platformProfile.userId = dbUser.id;

                    var oldProfile = await profilesService.findByPlatformUser(platformProfile.platformUserId, PLATFORMS.YOUTUBE, false, false);
                    //if (!(await profilesService.exist(platformProfile))) 
                    if (oldProfile.isEmpty()) {
                        //PROFILE NOT EXIST - save new to db
                        dbProfile = await profilesService.create(platformProfile);
                    }
                    else {
                        //PROFILE EXIST - update it
                        if (!oldProfile.active || oldProfile.userId == dbUser.id) {
                            platformProfile.id = oldProfile.id;
                            platformProfile.active = true;
                            dbProfile = await profilesService.update(platformProfile);
                        }
                        else {
                            //cannot connect because is connected to other account
                            done(new UserError('This YouTube account is already connected to another BitTip account!'));
                        }
                    }
                }
            }
            else {
                //NEW USER
                var platformProfile = getProfile(googleProfile, accessToken, refreshToken, params);

                var { user: dbUser, profile: dbProfile } = await userService.findOrCreateByPlatformProfile(platformProfile);

                //VIDEOS SYNC
                // ytService.syncVideos(profile).then(_ => {
                //     console.log("videos was successfully synchronized");
                // });
            }

            //find out ChannelId
            if (dbProfile.contentMasterId == dbProfile.platformUserId) {
                ytService.getFirstChannel(dbProfile.accessToken).then(channel => {
                    if (!channel.isEmpty() && channel.id) {
                        dbProfile.contentMasterId = channel.id;
                        profilesService.update(dbProfile);
                    }
                })
            }

            if (dbUser)
                done(null, dbUser);
            else
                done(null, false);
        }
        catch (err) {
            done(err);
        }
    }
));

function getProfile(ytProfile, accessToken, refreshToken, params) {
    var newProfile = new Profile({
        platformUserId: ytProfile.id,
        displayName: ytProfile.displayName,
        fullName: (
            (ytProfile.name.givenName ? ytProfile.name.givenName : '') +
            ' ' +
            (ytProfile.name.familyName ? ytProfile.name.familyName : ytProfile.name.familyName)
        ).trim(),
        email: ytProfile.emails[0].value,
        emailVerified: ytProfile.emails[0].value.endsWith('@pages.plusgoogle.com') ? false : ytProfile.emails[0].verified, //@pages.plusgoogle.com always not verified
        platform: PLATFORMS.YOUTUBE,
        imgUrl: ytProfile.photos[0].value,
        accessToken: accessToken,
        expireDate: moment().add(params.expires_in, 's').toDate(),
        refreshToken: refreshToken
    });

    //validate profile
    var validResult = newProfile.isValid();
    if (validResult.error)
        throw new Error(validResult.error);

    return newProfile;
}

module.exports = (options) => {
    return passport.authenticate('google', options);
}