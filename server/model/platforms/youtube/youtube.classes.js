const { PLATFORMS } = require('../../enums');
const BaseObject = require('../../BaseObject');
const Content = require('../../contents/Content');

class Video extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.privateId = data.id;
            this.publicId = data.snippet?.resourceId?.videoId ? data.snippet?.resourceId?.videoId : data.id;
            this.publishedAt = data.snippet?.publishedAt ? new Date(data.snippet?.publishedAt) : undefined;
            this.channelId = data.snippet?.channelId;
            this.title = data.snippet?.title;
            this.description = data.snippet?.description;
        }
    }

    toContent(profileId) {
        var content = new Content();
        content.privateId = this.privateId;
        content.publicId = this.publicId;
        content.masterId = this.channelId;
        content.profileId = profileId;
        content.platform = PLATFORMS.YOUTUBE;
        content.url = this.publicId ? `https://www.youtube.com/watch?v=${this.publicId}` : null;
        content.title = this.title;
        content.description = this.description;
        content.publishDate = this.publishedAt;
        return content;
    }

    static listFromData(listOfData) {
        var videos = [];

        if (!Array.isArray(listOfData) && listOfData.hasOwnProperty("items"))
            listOfData = listOfData.items;

        if (Array.isArray(listOfData)) {
            listOfData.forEach(videoData => {
                videos.push(new Video(videoData));
            });
        }

        return videos;
    }

}

class Channel extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.id = data.id;
            this.title = data.snippet?.title;
            this.description = data.snippet?.description;
            this.publishedAt = data.snippet?.publishedAt ? new Date(data.snippet?.publishedAt) : undefined;
            this.uploadsPaylistId = data.contentDetails?.relatedPlaylists?.uploads;
        }
    }

    static listFromData(listOfData) {
        var channels = [];

        if (!Array.isArray(listOfData) && listOfData.hasOwnProperty("items"))
            listOfData = listOfData.items;

        if (Array.isArray(listOfData)) {
            listOfData.forEach(chanelData => {
                channels.push(new Channel(chanelData));
            });
        }

        return channels;
    }

}

module.exports = {
    Video,
    Channel
}
