const { PLATFORMS } = require('../enums');
const youtubeService = require('./youtube/youtube.service');
const stackService = require('./stackexchange/stackexchange.service');
const mediumService = require('./medium/medium.service');
const Content = require('../contents/Content');

/**
 * find content online on platform
 * @param {Number} contentPublicId 
 * @param {PLATFORMS} platform 
 * @returns 
 */
async function findContent(contentPublicId, platform, extraData){
    var content = new Content();

    switch (platform) {
        case PLATFORMS.YOUTUBE:
            return youtubeService.findContent(contentPublicId);
        case PLATFORMS.STACKEXCHANGE:
            return stackService.findContent(contentPublicId, extraData.type, extraData.site);
        case PLATFORMS.MEDIUM:
            return mediumService.findContent(contentPublicId);
        default:
            return content;
    }
}

module.exports = {
    findContent
}