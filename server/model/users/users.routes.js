const router = require('express').Router();
const userService = require('./users.service');
const { auth } = require('../auth/auth.jwt');
const { filterList } = require('./users.permission');
const asyncHandler = require('express-async-handler')

router.post('/', auth, asyncHandler(async (req, res) => {
    var users = await userService.list(true)
    users = filterList(req.user, users);
    res.send(users);
}));


router.post('/me', auth, asyncHandler(async (req, res) => {
    var me = await userService.findById(req.user.id, true);
    res.send(me);
}));

router.post('/avatar', auth, asyncHandler(async (req, res) => {
    var url = getRandomAvatarUrl();
    res.redirect(url);
}));

module.exports = router;