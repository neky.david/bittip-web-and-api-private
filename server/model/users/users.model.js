const db = require('../../services/db.service');
const User = require('./User');

//ESCAPING USER INPUT TO PREVENT INJECTIONS!!!
// use ?
// or mysql.escape(input)

module.exports = {

    getAll: async (toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? User.columnsToView : '*'}
            FROM users`;
        if (onlyActive) sql += ' AND active = 1';
        return await db.query(sql);
    },

    getById: async (id, toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? User.columnsToView : '*'}
            FROM users WHERE id = ?`;
        if (onlyActive) sql += ' AND active = 1';
        return await db.query(sql, id, { firstOrNull: true });
    },

    getByEmail: async (email, toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? User.columnsToView : '*'}
            FROM users WHERE email = ?`;
        if (onlyActive) sql += ' AND active = 1';
        return await db.query(sql, email, { firstOrNull: true });
    },

    /**
     * 
     * @param {User} user 
     * @returns 
     */
    insert: async (user, toView) => {
        let sql = 'INSERT INTO users SET ?';
        var result = await db.query(sql, user);
        user.id = result.insertId;
        user.password = null;
        if(toView) user = user.toView();
        return user;
    }

}