const UserModel = require('./users.model');
const profileService = require('../profiles/profiles.service');
const User = require('./User');
const objectHelper = require('../../helpers/object.helper');
const Profile = require('../profiles/Profile');
const profilesService = require('../profiles/profiles.service');
const fileService = require('../files/file.service');
const { v4: uuidv4 } = require('uuid');
const path = require('path');
const walletService = require('../wallets/wallets.service');

/**
 * Get list of all users
 * @returns {User[]} list of all users
 */
async function list(toView) {
    var data = await UserModel.getAll(toView);
    var users = objectHelper.toListOfObject(data, User);
    return users;
};

/**
 * Get user by ID
 * @param {number} id 
 * @returns {User} user
 */
async function findById(id, toView) {
    var user = new User(await UserModel.getById(id, toView));
    return user;
};

/**
 * Get User by EMAIL
 * @param {string} email 
 * @returns {User} user
 */
async function findByEmail(email, toView) {
    var user = new User(await UserModel.getByEmail(email, toView));
    return user;
};

/**
 * Save new user to database
 * @param {User} user 
 * @returns {User} created user
 */
async function create(user) {
    user.role = 'USER';
    user.active = true;
    if (!user.emailVerified) user.emailVerified = false;
    if (user.email && user.password) user.isLoginable = true;

    var randImg = false;
    if (!user.imgUrl) {
        user.imgUrl = getRandomAvatarUrl();
        randImg = true;
    }

    try {
        var fileName = uuidv4() + (randImg ? ".svg" : "");
        var filePath = path.join(path.dirname(require.main.filename), 'public/user_images', fileName);
        fileService.saveFileFromUrl(filePath, user.imgUrl);
        user.imgUrl = '/files/img/' + fileName;
    }
    catch (err) {
        user.imgUrl = null;
        console.log(err);
    }

    //create wallet
    var wallet = await walletService.createEmpty();

    //create user
    user.walletId = wallet.id;
    var newUser = await UserModel.insert(user);

    return newUser;
};

/**
 * Find or create user
 * @param {User} user 
 * @param {Boolean} toView
 * @returns {User} user
 */
async function findOrCreate(user, toView) {
    var dbUser = new User();
    //find by ID
    if (user.id)
        dbUser = await this.findById(user.id, toView);
    //find by EMAIL
    else if (user.email)
        dbUser = await this.findByEmail(user.email, toView);

    if (dbUser.isEmpty()) {
        dbUser = await this.create(user);
    }

    return dbUser;
};

/**
 * Find user by platformUserId and platform or email and update it.. if not, create new user and profile
 * 
 * @param {Profile} profile 
 * @returns {User, Profile} {user, profile}
 */
async function findOrCreateByPlatformProfile(profile) {
    var dbUser = null;

    var dbProfile = await profileService.findByPlatformUser(profile.platformUserId, profile.platform, false, false);

    //PROFILE and USER EXIST
    if (!dbProfile.isEmpty() && dbProfile.active) {
        await profileService.update(profile);
        //return user
        dbUser = await this.findById(dbProfile.userId);
    }
    else {
        var newUser = profile.toUser();
        newUser.isLoginable = false;
        dbUser = await this.create(newUser); //findOrCreate?

        profile.userId = dbUser.id;
        if (dbProfile.isEmpty()) {
            //CREATE new profile
            dbProfile = await profilesService.create(profile);
        }
        else {
            //UPDATE exting profile
            profile.id = dbProfile.id;
            profile.active = true;
            dbProfile = await profilesService.update(profile);
        }
    }

    return { user: dbUser, profile: dbProfile };
};

function getRandomAvatarUrl() {
    //(["])(?:(?=(\\?))\2.)*?\1
    var url = 'https://avataaars.io/?avatarStyle=Transparent'

    var array = ['NoHair', 'Eyepatch', 'Hat', 'Hijab', 'Turban', 'WinterHat1', 'WinterHat2', 'WinterHat3', 'WinterHat4', 'LongHairBigHair', 'LongHairBob', 'LongHairBun', 'LongHairCurly', 'LongHairCurvy', 'LongHairDreads', 'LongHairFrida', 'LongHairFro', 'LongHairFroBand', 'LongHairNotTooLong', 'LongHairShavedSides', 'LongHairMiaWallace', 'LongHairStraight', 'LongHairStraight2', 'LongHairStraightStrand', 'ShortHairDreads01', 'ShortHairDreads02', 'ShortHairFrizzle', 'ShortHairShaggyMullet', 'ShortHairShortCurly', 'ShortHairShortFlat', 'ShortHairShortRound', 'ShortHairShortWaved', 'ShortHairSides', 'ShortHairTheCaesar', 'ShortHairTheCaesarSidePart'];
    url += "&topType=" + array[Math.floor(Math.random() * array.length)];

    array = ['Blank', 'Kurt', 'Prescription01', 'Prescription02', 'Round', 'Sunglasses', 'Wayfarers'];
    url += "&accessoriesType=" + array[Math.floor(Math.random() * array.length)];

    array = ['Blank', 'BeardMedium', 'BeardLight', 'BeardMajestic', 'MoustacheFancy', 'MoustacheMagnum', 'Blank', 'Blank', 'Blank', 'Blank', 'Blank', 'Blank', 'Blank', 'Blank', 'Blank', 'Blank', 'Blank'];
    url += "&facialHairType=" + array[Math.floor(Math.random() * array.length)];

    array = ['Auburn', 'Black', 'Blonde', 'BlondeGolden', 'Brown', 'BrownDark', 'Platinum', 'Red'];
    url += "&facialHairColor=" + array[Math.floor(Math.random() * array.length)];

    array = ['BlazerShirt', 'BlazerSweater', 'CollarSweater', 'GraphicShirt', 'Hoodie', 'Overall', 'ShirtCrewNeck', 'ShirtScoopNeck', 'ShirtVNeck'];
    url += "&clotheType=" + array[Math.floor(Math.random() * array.length)];

    array = ['Black', 'Blue01', 'Blue02', 'Blue03', 'Gray01', 'Gray02', 'Heather', 'PastelBlue', 'PastelGreen', 'PastelOrange', 'PastelRed', 'PastelYellow', 'Pink', 'Red', 'White'];
    url += "&clotheColor=" + array[Math.floor(Math.random() * array.length)];

    array = ['Bat', 'Cumbia', 'Deer', 'Diamond', 'Hola', 'Pizza', 'Resist', 'Selena', 'Bear', 'SkullOutline', 'Skull'];
    url += "&graphicType=" + array[Math.floor(Math.random() * array.length)];

    array = ['Default', 'Happy', 'Side', 'Wink']; //'Close','Cry','Dizzy','EyeRoll','Hearts','Squint','Surprised','WinkWacky'
    url += "&eyeType=" + array[Math.floor(Math.random() * array.length)];

    array = ['Default', 'DefaultNatural', 'FlatNatural', 'RaisedExcited', 'RaisedExcitedNatural', 'SadConcerned', 'SadConcernedNatural', 'UpDown', 'UpDownNatural']; //'Angry','AngryNatural','UnibrowNatural'
    url += "&eyebrowType=" + array[Math.floor(Math.random() * array.length)];

    array = ['Default', 'Serious', 'Smile', 'Twinkle']; //'Concerned','Disbelief','Eating','Grimace','Sad','ScreamOpen','Tongue','Vomit'
    url += "&mouthType=" + array[Math.floor(Math.random() * array.length)];

    array = ['Tanned', 'Yellow', 'Pale', 'Light', 'Brown', 'DarkBrown', 'Black'];
    url += "&skinColor=" + array[Math.floor(Math.random() * array.length)];

    return url;
}

module.exports = {
    list,
    findById,
    findByEmail,
    create,
    findOrCreate,
    findOrCreateByPlatformProfile,
    getRandomAvatarUrl
};