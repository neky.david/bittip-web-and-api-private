const { ROLE } = require('../../model/enums')
const User = require('./User')

/**
 * 
 * @param {User} currentUser 
 * @param {User} userToView 
 * @returns 
 */
function canView(currentUser, userToView) {
    return (
        currentUser.role === ROLE.ADMIN ||
        userToView.userId === currentUser.id
    )
}

/**
 * filter users that not allowed to show to current user
 * 
 * @param {User} currentUser 
 * @param {User[]} usersToView 
 * @returns 
 */
function filterList(currentUser, usersToView) {
    if (!usersToView || currentUser.role === ROLE.ADMIN) return usersToView
    return usersToView.filter(user => user.id === currentUser.id)
}

/**
 * 
 * @param {User} currentUser 
 * @param {User} userToDelete 
 * @returns 
 */
function canDelete(currentUser, userToDelete) {
    return userToDelete.id === currentUser.id
}

module.exports = {
    canView,
    canDelete,
    filterList
}