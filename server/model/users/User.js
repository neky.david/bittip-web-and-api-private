const Joi = require('joi');
const BaseObject = require('../BaseObject');

class User extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.id = this.displayName = this.fullName = this.email = this.emailVerified = this.password = this.isLoginable = this.walletId =
                this.role = this.imgUrl = this.createDate = this.updateDate = this.active = undefined;
            Object.assign(this, data);
        }
    }

    isValid() {
        return validSchema.validate(this);
    }

    static columnsToView = 'id, display_name, full_name, role, img_url';

    toView() {
        return new User({
            id: this.id,
            displayName: this.displayName,
            fullName: this.fullName,
            role: this.role,
            imgUrl: this.imgUrl
        })
    }
}

const validSchema = Joi.object({
    id:
        Joi.number().integer().positive().max(99999999999)
            .allow(null).optional(), //allow(null) => null.. optional() => undefined,
    displayName:
        Joi.string()
            .required(),
    fullName:
        Joi.string()
            .allow(null).optional(),
    email:
        Joi.string().email()
            .required(),
    emailVerified:
        Joi.boolean().falsy('false').truthy('true')
            .allow(null).optional(),
    role:
        Joi.string().allow('USER', 'ADMIN')
            .required(),
    password:
        Joi.string()
            .allow(null).optional(),
    isLoginable:
        Joi.bool()
            .allow(null).optional(),
    walletId:
        Joi.number().integer().positive().max(99999999999)
            .required(),
    imgUrl:
        Joi.string()
            .allow(null).optional(),
    active:
        Joi.bool()
            .allow(null).optional(),
});

module.exports = User