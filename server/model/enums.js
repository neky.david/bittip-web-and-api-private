module.exports = {
    ROLE: Object.freeze({
        'ADMIN': 'ADMIN',
        'USER': 'USER'
    }),

    PLATFORMS: Object.freeze({
        'YOUTUBE': 'YOUTUBE',
        'MEDIUM': 'MEDIUM',
        'STACKEXCHANGE': 'STACKEXCHANGE'
    }),

    PLATFORMS_LOGINABLE: Object.freeze({
        'YOUTUBE': 'YOUTUBE',
        'STACKEXCHANGE': 'STACKEXCHANGE'
    })
}