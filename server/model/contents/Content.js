const Joi = require('joi');
const BaseObject = require('../BaseObject');

class Content extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.id = this.publicId = this.privateId = this.masterId = this.profileId = this.platform = this.url =
                this.title = this.description = this.publishDate = this.createDate = this.updateDate = this.active = undefined;
            Object.assign(this, data);
        }
    }

    isValid() {
        return validSchema.validate(this);
    }

    static columnsToView = 'id, public_id, platform, url, title, description, active';

    toView() {
        return new Content({
            id: this.id,
            publicId: this.publicId,
            platform: this.platform,
            url: this.url,
            title: this.title,
            description: this.description,
            active: this.active
        })
    }
}

const validSchema = Joi.object({
    id:
        Joi.number().integer().positive().max(99999999999)
            .allow(null).optional(), //allow(null) => null.. optional() => undefined
    publicId:
        Joi.string()
            .required(),
    privateId:
        Joi.string()
            .required(),
    masterId:
        Joi.string()
            .required(),
    profileId:
        Joi.number().integer().positive().max(99999999999)
            .required(),
    platform:
        Joi.string()
            .required(),
    url:
        Joi.string()
            .allow(null).optional(),
    title:
        Joi.string()
            .allow(null).optional(),
    description:
        Joi.string()
            .allow(null).optional(),
    publishDate:
        Joi.date()
            .allow(null).optional(),
    active:
        Joi.bool()
            .allow(null).optional(),
});

module.exports = Content;