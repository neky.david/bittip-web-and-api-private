const db = require('../../services/db.service');
const Content = require('./Content');

//ESCAPING USER INPUT TO PREVENT INJECTIONS!!!
//use ? OR mysql.escape(input)

module.exports = {

    getById: async (contentId, toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? Content.columnsToView : '*'} 
            FROM contents where id = ?`;
        if (onlyActive) sql += ' AND active = 1';
        return await db.query(sql, contentId, { firstOrNull: true });
    },

    getByPublicIdAndPlatform: async (contentPublicId, platform, toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? Content.columnsToView : '*'}  
            FROM contents where public_id = ? AND platform = ?`;
        if (onlyActive) sql += ' AND active = 1';
        return await db.query(sql, [contentPublicId, platform], { firstOrNull: true });
    },

    getByProfileId: async (profileId, toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? Content.columnsToView : '*'} 
            FROM contents where profile_id = ?`;
        if (onlyActive) sql += ' AND active = 1';
        return await db.query(sql, profileId);
    },

    insert: async (content, toView) => {
        let sql = 'INSERT INTO contents SET ?';
        var result = await db.query(sql, content);
        content.id = result.insertId;
        if (toView) content = content.toView();
        return content;
    }

}