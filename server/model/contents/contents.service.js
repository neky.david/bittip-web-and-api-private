const model = require('./contents.model');
const Content = require('./Content');
const platformService = require('../platforms/platforms.service');
const { PLATFORMS } = require('../enums');

async function findById(contentId) {
    return new Content(await model.getById(contentId));
};

async function findByPublicIdAndPlatform(contentPublicId, platform) {
    return new Content(await model.getByPublicIdAndPlatform(contentPublicId, platform));
};

async function findByProfileId(profileId) {
    return new Content(await model.getByProfileId(profileId));
};

/**
 * find content online (platform API)
 * @param {Number} contentPublicId 
 * @param {PLATFORMS} platform 
 */
async function LoadFromPlatform(contentPublicId, platform, extraData) {
    return await platformService.findContent(contentPublicId, platform, extraData)
}

/**
 * Find Content in DB or load it from platform API and save it to DB
 * @param {number} contentPublicId 
 * @param {PLATFORMS} platform 
 * @param {object} extraData 
 * @returns 
 */
async function findOrLoadFromPlatform(contentPublicId, platform, extraData) {
    var content = await findByPublicIdAndPlatform(contentPublicId, platform)
    if (content.isEmpty()) {
        //NOT in DB - find on platform
        content = await LoadFromPlatform(contentPublicId, platform, extraData);
        // if (!content.isEmpty()) {
        //     //SAVE to DB
        //     content = await this.create(content);
        // }
    }
    return content;
}

async function create(content) {
    var dbContent = await this.findByPublicIdAndPlatform(content.publicId, content.platform);
    if (dbContent.isEmpty())
        dbContent = await model.insert(content);
    return dbContent;
};


module.exports = {
    findById,
    findByPublicIdAndPlatform,
    findByProfileId,
    create,
    LoadFromPlatform,
    findOrLoadFromPlatform


    // findOrCreateProfile: async (profile) => {
    //     var dbProfile = ProfileModel.getById(profile.id);
    //     //profile exist
    //     if(dbProfile)
    //         return dbProfile;

    //     //create new user and profile
    //     profile.user_id = UserService.createFromProfile(profile);
    //     ProfileModel.insert(profile);
    // }

}