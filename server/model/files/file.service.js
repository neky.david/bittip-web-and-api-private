const fs = require('fs');
const axios = require('axios');

function readFile(filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, function (err, data) {
            if (err)
                reject(err);
            else
                resolve(data);
        });
    });
}

async function writeFile(filePath, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath, data, function (err) {
            if (err)
                reject(err);
            else
                resolve()
        });
    });
}

async function saveFileFromUrl(filePath, url) {  
    const writer = fs.createWriteStream(filePath)
  
    const response = await axios({
      url,
      method: 'GET',
      responseType: 'stream'
    })
  
    response.data.pipe(writer)
  
    return new Promise((resolve, reject) => {
      writer.on('finish', resolve)
      writer.on('error', reject)
    })
  }

// async function saveFileFromUrl(filePath, url) {
//     var res = await axios.get(url,
//         {
//         responseType: 'arraybuffer'
//         }
//     );
//     if (res.data) {
//         var buffer = Buffer.from(res.data, 'binary').toString('base64')
//         //buffer = "data:image/png;base64, " + buffer;
//         await writeFile(filePath, buffer);
//     }
// }

module.exports = {
    readFile,
    writeFile,
    saveFileFromUrl
}