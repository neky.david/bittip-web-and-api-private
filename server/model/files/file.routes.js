const router = require('express').Router();
const fileService = require('./file.service');
const path = require('path');
const asyncHandler = require('express-async-handler')

router.get('/img/:name', asyncHandler(async (req, res, next) => {
    var options = {
        root: path.join(path.dirname(require.main.filename), 'public/user_images'),
        dotfiles: 'deny',
        headers: {
          'x-timestamp': Date.now(),
          'x-sent': true
        }
      }

      var fileName = req.params.name;
      res.sendFile(fileName, options, function (err) {
        if (err) {
          next(err)
        } else if(process.env.TESTING) {
           console.log('File sended:', fileName)
        }
      })
}));

router.get('/img/platform/:name', asyncHandler(async (req, res, next) => {
  var options = {
      root: path.join(path.dirname(require.main.filename), 'public/platforms'),
      dotfiles: 'deny',
      headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
      }
    }

    var fileName = req.params.name;
    res.sendFile(fileName, options, function (err) {
      if (err) {
        next(err)
      } else if(process.env.TESTING) {
        console.log('File sended:', fileName)
      }
    })
}));

module.exports = router;