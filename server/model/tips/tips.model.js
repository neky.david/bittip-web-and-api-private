const db = require('../../services/db.service');
const Payment = require('../payments/Payment');
const Tip = require('./Tip');

module.exports = {

    getById: async (tipId, toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? Tip.columnsToView : 't.*'} 
            FROM tips AS t WHERE t.id = ?`;
        if (onlyActive) sql += ' AND t.active = 1';
        return await db.query(sql, tipId, { firstOrNull: true });
    },

    /**
     * 
     * @param {Tip} tip 
     * @param {boolean} toView 
     * @returns 
     */
    insert: async (tip, toView) => {
        let sql = 'INSERT INTO tips SET ?';
        var result = await db.query(sql, tip);
        tip.id = result.insertId;
        if(toView) tip = tip.toView();
        return tip;
    },

    getListPayedByUserId: async(userId, count, toView) => {
        let sql = `
            SELECT 
            ${toView ? Tip.columnsToView + ', ' + Payment.columnsToView : 't.*, p.*'}
            FROM tips AS t
            JOIN payments AS p ON p.id = t.payment_id
            WHERE t.recipient_user_id = ? AND p.payed = 1 AND t.active = 1
            ORDER BY p.pay_date DESC
            ${count ? 'LIMIT ?' : ''}`;
        var tips = await db.query(sql, [userId, count]);
        return tips;
    },

    /**
     * 
     * @param {Tip} tip 
     */
    update: async (tip) => {
        let sql = `
            UPDATE tips SET 
                sender_user_id = COALESCE(?, sender_user_id),
                content_id = COALESCE(?, content_id),
                message = COALESCE(?, message),
                value = COALESCE(?, value),
                payed = COALESCE(?, payed),
                pay_date = COALESCE(?, pay_date),
                memo = COALESCE(?, memo),
                active = COALESCE(?, active)
            WHERE id = ?`;
        var result = await db.query(sql, [tip.senderUserId, tip.contentId, tip.message, tip.value, 
            tip.payed, tip.payDate.toSqlFormat(), tip.memo, tip.active, tip.id]);
    },

}