const model = require('./tips.model');
const Tip = require('./Tip');
const { PLATFORMS } = require('../enums');
const User = require('../users/User');
const contentService = require('../contents/contents.service');
const profileService = require('../profiles/profiles.service');
const UserError = require('../../errors/UserError');
const { HttpStatusCode, ErrorType } = require('../../errors/error.enums');
const tipsModel = require('./tips.model');
const walletService = require('../wallets/wallets.service');
const objectHelper = require('../../helpers/object.helper');
const paymentService = require('../payments/payments.service');
const usersService = require('../users/users.service');
const dbService = require('../../services/db.service');
const Payment = require('../payments/Payment');

async function findById(tipId, toView) {
    return new Tip(await model.getById(tipId, toView));
};

async function update(tip) {
    if (tip.id)
        await tipsModel.update(tip);
}

async function listPayedByUserId(userId, count, toView) {
    var data = await tipsModel.getListPayedByUserId(userId, count, toView);
    return objectHelper.toListOfObject(data, Tip);
}

/**
 * It will save new tip a created invoice payment
 * @returns {Tip} new tip with payment variable
 */
async function createTip(contentId, recipientWalletId, recipientUserId, recipientProfileId, senderUserId, memo, platform, toView) {

    //Create invoice PAYMENT
    var payment = await paymentService.createIncomingPayment(recipientWalletId, memo, 0, toView);

    var tip = new Tip();
    tip.paymentId = payment.id;
    tip.contentId = contentId;
    tip.recipientUserId = recipientUserId;
    tip.recipientProfileId = recipientProfileId;
    tip.senderUserId = senderUserId || undefined;
    tip.platform = platform;

    tip = await tipsModel.insert(tip, toView);
    tip.payment = payment;
    return tip;
}

/**
 * Create new tip & payment from contentId and Platform
 * @param {Number} contentId 
 * @param {PLATFORMS} platform 
 * @param {User} currentUser 
 * @returns {{Tip, Content, Profile}} object with tip, content and profile
 */
async function createFromPlatformContent(contentPublicId, platform, extraData, currentUser) {
    var content = await contentService.findOrLoadFromPlatform(contentPublicId, platform, extraData);
    if (!content.isEmpty()) {
        var profile = await profileService.findByContentMasterId(content.masterId, platform, true);
        if (!profile.isEmpty()) {
            try {
                dbService.beginTransaction();
                //content is not save
                if (!content.id) {
                    content.profileId = profile.id;
                    content = await contentService.create(content);
                }
                content = content.toView();

                var user = await usersService.findById(profile.userId);

                var memo = "BitTip - " + (content.title ? content.title : content.description)
                var tip = await this.createTip(content.id, user.walletId, user.id, profile.id, currentUser?.id || undefined, memo, platform, true);

                dbService.commitTransaction();

                return { tip, content, user, profile };
            }
            catch (err) {
                dbService.rollbackTransaction();
                console.error(err);
                throw new UserError("", HttpStatusCode.BAD_REQUEST, "Invoice creating failed");
            }
        }
        else {
            throw new UserError(`Publisher of this content is not registred on BitTip.`, HttpStatusCode.BAD_REQUEST, "User is not registred", ErrorType.INFO);
        }
    }
    else {
        throw new UserError(`Cannot find content on ${platform} with identifikator ${contentPublicId}`, HttpStatusCode.BAD_REQUEST, "Content not found", ErrorType.WARNING);
    }
}

/**
 * Wait until tip was payed and then return payment object
 * 
 * @param {Number} tipId 
 * @param {Boolean} toView 
 * @returns {Payment} payment
 */
async function waitToPayTip(tipId, toView) {
    var payment = await paymentService.findByTipId(tipId, false);
    if (!payment.isEmpty()) {
        payment = await paymentService.waitToPay(payment.rHash, toView);
        return payment;
    }
}

module.exports = {
    findById,
    listPayedByUserId,
    createFromPlatformContent,
    createTip,
    waitToPayTip,
    update,
}