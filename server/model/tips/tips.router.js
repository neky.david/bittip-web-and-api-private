const router = require('express').Router();
const asyncHandler = require('express-async-handler')
const tipService = require('./tips.service');
const usersService = require('../users/users.service');
const { getCurrentUserFromCookie, auth } = require('../auth/auth.jwt');

router.post('/platform', asyncHandler(async (req, res) => {
    if (req.body && req.body.platform && req.body.contentPublicId) {

        var { tip, content, user, profile } = await tipService.createFromPlatformContent(
            req.body.contentPublicId,
            req.body.platform.toUpperCase(),
            req.body.extraData,
            await getCurrentUserFromCookie(req));

        tip.content = content;
        tip.user = user;
        tip.profile = profile;

        res.send(tip);
    }
    else {
        throw new Error("ContentId and Platform is required!")
    }
}));


router.post('/waitToPay', asyncHandler(async (req, res) => {
    if (req.body && req.body.tipId) {

        var payment = await tipService.waitToPayTip(req.body.tipId, true);

        res.send(payment);
    }
    else {
        throw new Error("TipId is required!")
    }
}));

router.post('/myTips', auth, asyncHandler(async (req, res) => {
    var tips = await tipService.listPayedByUserId(req.user.id, req.body.count, true);
    res.send(tips);
}));

module.exports = router;