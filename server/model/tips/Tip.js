const Joi = require('joi');
const BaseObject = require('../BaseObject');

class Tip extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.id = this.recipientUserId = this.senderUserId = this.recipientProfileId = this.contentId = this.paymentId = this.payment
            this.message = this.platform = this.active = this.createDate = this.updateDate = undefined;
            Object.assign(this, data);
        }
    }

    isValid() {
        return validSchema.validate(this);
    }

    static columnsToView = 't.id, t.message, t.platform, t.active';

    toView() {
        return new Tip({
            id: this.id,
            message: this.message,
            platform: this.platform,
            active: this.active
        })
    }
}

const validSchema = Joi.object({
    id:
        Joi.number().integer().positive().max(99999999999)
            .allow(null).optional(), //allow(null) => null.. optional() => undefined
    recipientUserId:
        Joi.number().integer().positive().max(99999999999)
            .required(),
    senderUserId:
        Joi.number().integer().positive().max(99999999999)
            .allow(null).optional(),
    paymentId:
        Joi.number().integer().positive().max(99999999999)
            .required(),
    message:
        Joi.string()
            .allow(null).optional(),
    platform:
        Joi.string()
            .required(),
    createDate:
        Joi.date()
            .allow(null).optional(),
    updateDate:
        Joi.date()
            .allow(null).optional(),
    active:
        Joi.bool()
            .allow(null).optional(),
});

module.exports = Tip;