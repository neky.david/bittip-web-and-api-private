const model = require('./ext_param.model');
const ExtParam = require('./ExtParam');
const objectHelper = require('../../helpers/object.helper');

async function findById(extParamId) {
    return new ExtParam(await model.getById(extParamId));
};

async function findByNameAndEntity(name, entityId, entityType) {
    return new ExtParam(await model.getByNameAndEntity(name, entityId, entityType));
};

async function listByEntity(entityId, entityType) {
    var params = await model.getListByEntity(entityId, entityType);
    return objectHelper.toListOfObject(params, ExtParam);
};

/**
 * 
 * @param {ExtParam} extParam 
 * @returns 
 */
async function createOrUpdate(extParam) {
    //check if param with same name exist
    var dbParam = await this.findByNameAndEntity(extParam.name, extParam.entityId, extParam.entityType)
    if (dbParam.isEmpty())
        return await model.insert(extParam);
    else
        return await model.update(extParam);
};


module.exports = {
    findById,
    findByNameAndEntity,
    listByEntity,
    createOrUpdate
}