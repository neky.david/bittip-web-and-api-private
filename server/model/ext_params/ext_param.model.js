const db = require('../../services/db.service');
const ExtParam = require('./ExtParam');

//ESCAPING USER INPUT TO PREVENT INJECTIONS!!!
//use ? OR mysql.escape(input)

module.exports = {

    getById: async (id) => {
        let sql = 'SELECT * FROM extension_params where id = ?';
        return await db.query(sql, id, { firstOrNull: true });
    },

    getByNameAndEntity: async (name, entityId, entityType) => {
        let sql = 'SELECT * FROM extension_params where name = ? AND entity_id = ? AND entity_type = ?';
        return await db.query(sql, [name, entityId, entityType], { firstOrNull: true });
    },

    getListByEntity: async (entityId, entityType) => {
        let sql = 'SELECT * FROM extension_params where entity_id = ? AND entity_type = ?';
        return await db.query(sql, [entityId, entityType]);
    },

    insert: async (extParam) => {
        extParam.valueType = extParam.value.constructor.name;
        let sql = 'INSERT INTO extension_params SET ?';
        var result = await db.query(sql, extParam);
        extParam.id = result.insertId;
        return extParam;
    },

    /**
     * 
     * @param {ExtParam} profile
     * @returns {ExtParam} updated param
     */
    update: async (extParam) => {
        extParam.valueType = extParam.value.constructor.name;
        let sql = `UPDATE extension_params SET
                value = ?,
                value_type = ?
                where name = ? AND entity_id = ? AND entity_type = ?`;
        var result = await db.query(sql, [extParam.value, extParam.valueType, extParam.name, extParam.entityId, extParam.entityType]);
        return extParam;
    }

    
}