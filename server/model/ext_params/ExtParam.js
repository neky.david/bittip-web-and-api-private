const Joi = require('joi');

class ExtParam {

    constructor(data) {
        if (data) {
            this.id = this.name = this.value = this.valueType = this.entityType = this.entityId = undefined;
            Object.assign(this, data);
        }
    }

    isEmpty() {
        return Object.keys(this).length === 0
    }

    isValid() {
        return validSchema.validate(this);
    }
}

const validSchema = Joi.object({
    id:
        Joi.number().integer().positive().max(99999999999)
            .allow(null).optional(), //allow(null) => null.. optional() => undefined
    name:
        Joi.string()
            .required(),
    value:
        Joi.string()
            .required(),
    valueType:
        Joi.string()
            .allow(null).optional(),
    entityType:
        Joi.string()
            .required(),
    entityId:
        Joi.number().integer().positive().max(99999999999)
            .required()
});

module.exports = ExtParam;