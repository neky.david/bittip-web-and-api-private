const Wallet = require('./Wallet');
const walletModel = require('./wallets.model');
const Tip = require('../tips/Tip');
const objectHelper = require('../../helpers/object.helper');
const lnurlService = require('../../services/lightning/lnurl.service');
const UserError = require('../../errors/UserError');
const { HttpStatusCode, ErrorType } = require('../../errors/error.enums');
const Payment = require('../payments/Payment');
const paymentService = require('../payments/payments.service');
const PaymentType = require('../payments/PaymentType');

/**
 * Get Wallet by ID
 * @param {number} id 
 * @returns {Wallet} wallet
 */
async function findById(id, toView) {
    var wallet = new Wallet(await walletModel.getById(id, toView));
    return wallet;
};

/**
 * Get Wallet by User ID
 * @param {string} email 
 * @returns {Wallet} wallet
 */
async function findByUserId(userId, toView) {
    var wallet = new Wallet(await walletModel.getByUserId(userId, toView));
    return wallet;
};

/**
 * Save new wallet to database
 * @param {Wallet} user 
 * @returns {Wallet} created wallet
 */
async function create(wallet, toView) {
    wallet.active = true;
    return await walletModel.insert(wallet, toView);
};

async function createEmpty() {
    var wallet = new Wallet();
    wallet.amount = 0;
    wallet.tipsCount = 0;
    return await create(wallet);
};

async function update(wallet) {
    if (wallet.id)
        await walletModel.update(wallet);
}

/**
 * 
 * @param {number} walletId 
 * @returns {Wallet[]} wallets
 */
async function findWalletHistoryById(walletId, count) {
    var data = await walletModel.getWalletHistoryById(walletId, count);
    var wallets = [];
    data.forEach(walletHist => {
        walletHist.id = walletHist.walletId;
        wallets.push(new Object(walletHist));
    });
    return wallets;
}

async function getWalletStatsById(walletId) {
    var wallets = await this.findWalletHistoryById(walletId, 20);
    var dayTips = [];
    var numTips = [];
    var avgTips = [];
    if (wallets.length > 0) {
        for (var i = 0; i < wallets.length; i++) {
            var currWallet = wallets[i];
            var prevWallet = i > 0 ? wallets[i - 1] : null;

            dayTips.push(currWallet.amount - (prevWallet ? prevWallet.amount : 0));
            numTips.push(currWallet.tipsCount - (prevWallet ? prevWallet.tipsCount : 0));
            avgTips.push(currWallet.tipsCount ? currWallet.amount / currWallet.tipsCount : 0);
        }
    }
    return { dayTips, numTips, avgTips };
}

/**
 * @param {Payment} payment 
 */
async function recalculateBalanceByPayment(payment) {
    if (process.env.TESTING) {
        console.log('recalculateBalanceByPayment');
        console.log(payment);
    }
    if (payment && !payment.isEmpty() && payment.payed && payment.walletId) {

        var wallet = await findById(payment.walletId);
        if (process.env.TESTING) { console.log(wallet); }
        if (!wallet.isEmpty()) {
            if (payment.type == PaymentType.INCOMING) {
                wallet.amount = parseInt(wallet.amount) + parseInt(payment.amount);
                wallet.tipsCount++;
            }
            else if (payment.type == PaymentType.OUTGOING) {
                wallet.withdrawAmount = parseInt(wallet.withdrawAmount) + parseInt(payment.amount);
            }
            if (process.env.TESTING) {
                console.log('WALLET BEFORE UPDATE');
                console.log(wallet);
            }
            this.update(wallet);
        }

    }
}

/*
async function withdraw(user) {
    var wallet = await findByUserId(user.id);
    if (!wallet.isEmpty() && wallet.amount && wallet.amount > 1000) {
        var result = await lnurlService.withdraw(10, wallet.amount * 1000, "BitTip Withdrawing");
        return result.encoded;
    }
    else {
        throw new UserError("You must have more than 10 sats in your wallet due to Lightning network fees.", HttpStatusCode.OK, "Lack of sats in the wallet.", ErrorType.WARNING);
    }
}
*/

module.exports = {
    findById,
    findByUserId,
    create,
    update,
    createEmpty,
    findWalletHistoryById,
    getWalletStatsById,
    recalculateBalanceByPayment
};