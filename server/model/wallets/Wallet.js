const Joi = require('joi');
const BaseObject = require('../BaseObject');

class Wallet extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.id = this.amount = this.withdrawAmount = this.tipsCount = this.userId = this.createDate = this.updateDate = this.active = undefined;
            Object.assign(this, data);
        }
    }

    isValid() {
        return validSchema.validate(this);
    }

    get availableAmount() { return this.amount - this.withdrawAmount - parseInt(process.env.WITHDRAW_FEE_RESERVE_MSATS || 10000) }

    static columnsToView = 'w.id, w.amount, w.withdraw_amount, w.tips_count, w.active, u.id AS user_id';

    toView() {
        return new Wallet({
            id: this.id,
            amount: this.amount,
            withdrawAmount: this.withdrawAmount,
            tipsCount: this.tipsCount,
            userId: this.userId,
            active: this.active
        })
    }
}

const validSchema = Joi.object({
    id:
        Joi.number().integer().positive().max(99999999999)
            .allow(null).optional(), //allow(null) => null.. optional() => undefined,
    amount:
        Joi.number().integer().positive().max(99999999999)
            .required(),
    withdrawAmount:
        Joi.number().integer().positive().max(99999999999)
            .required(),
    tipsCount:
        Joi.number().integer().positive().max(99999999999)
            .required(),
    userId:
        Joi.number().integer().positive().max(99999999999)
            .required(),
    createDate:
        Joi.date()
            .allow(null).optional(),
    updateDate:
        Joi.date()
            .allow(null).optional(),
});

module.exports = Wallet