const router = require('express').Router();
const { auth } = require('../auth/auth.jwt');
const asyncHandler = require('express-async-handler');
const walletService = require('./wallets.service');
const withdrawService = require('../payments/withdraw.service');

router.post('/myWallet', auth, asyncHandler(async (req, res) => {
    var wallet = await walletService.findByUserId(req.user.id, true);
    wallet.availableToWithdraw = wallet.availableAmount;

    if (req.body.withStats) {
        wallet.stats = await walletService.getWalletStatsById(wallet.id);
    }

    res.send(wallet);
}));

router.post('/withdraw', auth, asyncHandler(async (req, res) => {
    var encodedUrl = await withdrawService.withdraw(req.user);
    res.send(encodedUrl);
    //throw new UserError("Withdraw not supported yet. But working on it day and night...", null, null, ErrorType.INFO);
}));

router.post('/waitToWithdraw', auth, asyncHandler(async (req, res) => {
    var payment = await withdrawService.waitToWithdraw(req.user, true);

    if (payment && !payment.isEmpty())
        var wallet = await walletService.findById(payment.walletId, true);

    if (process.env.TESTING) {
        console.log("waitToWithdraw RESPONSE:");
        console.log(payment);
        console.log(wallet);
    }

    payment.wallet = wallet;
    res.send(payment);
}));


module.exports = router;