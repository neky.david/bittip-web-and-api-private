const db = require('../../services/db.service');
const Wallet = require('./Wallet');

//ESCAPING USER INPUT TO PREVENT INJECTIONS!!!
// use ?
// or mysql.escape(input)

module.exports = {

    getById: async (id, toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? Wallet.columnsToView : '*, u.id AS user_id'}
            FROM wallets AS w
            JOIN users AS u ON u.wallet_id = w.id
            WHERE w.id = ?`;
        if (onlyActive) sql += ' AND w.active = 1';
        return await db.query(sql, id, { firstOrNull: true });
    },

    getByUserId: async (userId, toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? Wallet.columnsToView : '*, u.id AS user_id'}
            FROM wallets AS w
            JOIN users AS u ON u.wallet_id = w.id
            WHERE u.id = ?`;
        if (onlyActive) sql += ' AND w.active = 1';
        return await db.query(sql, userId, { firstOrNull: true });
    },

    getWalletHistoryById: async(walletId, count) => {
        let sql = `
            SELECT * 
            FROM wallets_history 
            WHERE wallet_id = ?
            ${count ? 'LIMIT ' + count : ''}`;
        return await db.query(sql, walletId);
    },

    /**
     * 
     * @param {Wallet} wallet 
     * @returns 
    */
    insert: async (wallet, toView) => {
        let sql = 'INSERT INTO wallets SET ?';
        var result = await db.query(sql, wallet);
        wallet.id = result.insertId;
        if (toView) wallet = wallet.toView();
        return wallet;
    },

    /**
     * 
     * @param {Wallet} wallet 
     */
    update: async (wallet) => {
        let sql = `UPDATE wallets SET 
                amount = COALESCE(?, amount),
                withdraw_amount = COALESCE(?, withdraw_amount),
                tips_count = COALESCE(?, tips_count)
            WHERE id = ?`;
        var result = await db.query(sql, [wallet.amount, wallet.withdrawAmount, wallet.tipsCount, wallet.id]);
        //return true;
    }

}