const extParamService = require('../model/ext_params/ext_param.service');
const ExtParam = require('./ext_params/ExtParam');


module.exports = class BaseObject {

    async getExtParam(name) {
        if (name && this.id && this.constructor.name)
            return await extParamService.findByNameAndEntity(name, this.id, this.constructor.name);
    }

    async setExtParam(name, value) {
        if (name && value && this.id && this.constructor.name) {
            var param = new ExtParam({
                name: name,
                value: value,
                entityType: this.constructor.name,
                entityId: this.id
            });
            return await extParamService.createOrUpdate(param);
        }
    }

    isEmpty() {
        return Object.keys(this).length === 0
    }
}