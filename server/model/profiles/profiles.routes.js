const router = require('express').Router();
const profileService = require('./profiles.service');
const { auth } = require('../auth/auth.jwt');
const { filterList } = require('./profiles.permission');
const asyncHandler = require('express-async-handler');

router.post('/', auth, asyncHandler(async (req, res) => {
    var profiles = await profileService.listByUser(req.user.id, true);

    profiles = filterList(req.user, profiles);
    res.send(profiles);
}));

router.post('/disconnect', auth, asyncHandler(async (req, res) => {
    if (req.body && req.body.profileId) {
        await profileService.disconnect(req.body.profileId, req.user);
        res.send();
    }
    else {
        throw new Error("ProfileId is required");
    }
}));

module.exports = router;