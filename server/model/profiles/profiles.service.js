const profileModel = require('./profiles.model');
const Profile = require('./Profile');
const objectHelper = require('../../helpers/object.helper');
const permission = require('./profiles.permission');
const UserError = require('../../errors/UserError');
const { HttpStatusCode } = require('../../errors/error.enums');

async function findById(profileId, toView, onlyActive) {
    return new Profile(await profileModel.getById(profileId, toView, onlyActive));
}

async function findByPlatformUser(platformUserId, platform, toView, onlyActive) {
    var profile = new Profile(await profileModel.getByPlatformUserIdAndPlatform(platformUserId, platform, toView, onlyActive));
    return profile;
}

async function findByContentMasterId(contentMasterId, platform, toView, onlyActive) {
    var profile = new Profile(await profileModel.getByContentMasterIdAndPlatform(contentMasterId, platform, toView, onlyActive));
    return profile;
}

async function findByUser(userId, platform, toView, onlyActive) {
    var profile = new Profile(await profileModel.getByUserIdAndPlatform(userId, platform, toView, onlyActive));
    return profile;
}

async function listByUser(userId, toView, onlyActive) {
    var data = await profileModel.getByUserId(userId, toView, onlyActive);
    var profiles = objectHelper.toListOfObject(data, Profile);
    return profiles;
}

async function listLoginableByUser(userId, toView, onlyActive) {
    var data = await profileModel.getLoginableByUserId(userId, toView, onlyActive);
    var profiles = objectHelper.toListOfObject(data, Profile);
    return profiles;
}

/**
 * 
 * @param {Profile} profile 
 * @param {boolean} toView 
 * @returns 
 */
 async function create(profile, toView) {
    profile.active = true
    //if contentMasterId is null.. set to platformUserId
    if (!profile.contentMasterId) profile.contentMasterId = profile.platformUserId;

    var dbProfile = await profileModel.insert(profile, toView);

    return dbProfile;
}

async function update(profile, toView) {
    if (profile.id) {
        return await profileModel.update(profile, toView);
    }
}

async function remove(profile) {
    if (profile.id) {
        await profileModel.delete(profile.id);
    }
}

async function disconnect(profileId, currentUser) {
    var profile = await this.findById(profileId);

    if (profile.isEmpty())
        throw new Error(`Profile with id ${profileId} does not exist`);
    if (!permission.canDelete(currentUser, profile))
        throw new UserError('You have no permission to disconnect from this platform', HttpStatusCode.FORBIDDEN, 'Permission denied');

    var canDisconnect =
        currentUser.isLoginable ||
        !profile.isLoginable ||
        (await this.listLoginableByUser(currentUser.id)).length > 1

    if (canDisconnect) {
        this.remove(profile);
    }
    else {
        throw new UserError(
            `Can\'t disconnect from this platform. You would have no way to log in to current BitTip account!. 
        You can disconnect from this platform if you save password and email address.`,
            HttpStatusCode.BAD_REQUEST, 'Connot disconnect from last platform');
    }
}

async function exist(profile) {
    return await profileModel.exist(profile);
}

module.exports = {
    findById,
    findByPlatformUser,
    findByContentMasterId,
    findByUser,
    listByUser,
    listLoginableByUser,
    create,
    update,
    remove,
    disconnect,
    exist
}