const { objectPropsToArray: objPropsToArray } = require('../../helpers/object.helper');
const db = require('../../services/db.service');
const { PLATFORMS_LOGINABLE } = require('../enums');
const Profile = require('./Profile');

//ESCAPING USER INPUT TO PREVENT INJECTIONS!!!
// use ?
// or mysql.escape(input)


module.exports = {

    getById: async (profileId, toView, onlyActive = true) => {
        let sql = `
            SELECT  
            ${toView ? Profile.columnsToView : '*'}
            FROM profiles where id = ?`;
        if (onlyActive) sql += ' AND active = 1';
        return await db.query(sql, profileId, { firstOrNull: true });
    },

    getByPlatformUserIdAndPlatform: async (platformUserId, platform, toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? Profile.columnsToView : '*'}
            FROM profiles where platform_user_id = ? and platform = ?`;
        if (onlyActive) sql += ' AND active = 1';
        return await db.query(sql, [platformUserId, platform], { firstOrNull: true });
    },

    getByContentMasterIdAndPlatform: async (contentMasterId, platform, toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? Profile.columnsToView : '*'}
            FROM profiles where content_master_id = ? and platform = ?`;
        if (onlyActive) sql += ' AND active = 1';
        return await db.query(sql, [contentMasterId, platform], { firstOrNull: true });
    },

    getByUserIdAndPlatform: async (userId, platform, toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? Profile.columnsToView : '*'}
            FROM profiles where user_id = ? and platform = ?`;
        if (onlyActive) sql += ' AND active = 1';
        return await db.query(sql, [userId, platform], { firstOrNull: true });
    },

    getByUserId: async (userId, toView, onlyActive = true) => {
        let sql = `
            SELECT 
            ${toView ? Profile.columnsToView : '*'} 
            FROM profiles where user_id = ?`;
        if (onlyActive) sql += ' AND active = 1';
        return await db.query(sql, userId);
    },

    getLoginableByUserId: async (userId, toView, onlyActive = true) => {

        var platfomrsArray = objPropsToArray(PLATFORMS_LOGINABLE);
        var platforms = "'" + platfomrsArray.join("','") + "'";

        let sql = `
            SELECT 
            ${toView ? Profile.columnsToView : '*'} 
            FROM profiles 
            where user_id = ?
            AND platform IN(${platforms})
            `;
        if (onlyActive) sql += ' AND active = 1';

        return await db.query(sql, userId);
    },

    insert: async (profile, toView) => {
        let sql = 'INSERT INTO profiles SET ?';
        var result = await db.query(sql, profile);
        profile.id = result.insertId;
        if(toView) profile = profile.toView();
        return profile;
    },

    /**
     * 
     * @param {Profile} profile
     * @returns {boolean} success update
     */
    update: async (profile, toView) => {
        let sql = `UPDATE profiles SET 
                platform_user_id = COALESCE(?, platform_user_id),
                content_master_id = COALESCE(?, content_master_id),
                user_id = COALESCE(?, user_id),
                user_name = COALESCE(?, user_name),
                display_name = COALESCE(?, display_name),
                full_name = COALESCE(?, full_name),
                email = COALESCE(?, email),
                email_verified = COALESCE(?, email_verified),
                platform = COALESCE(?, platform),
                access_token = COALESCE(?, access_token),
                expire_date = COALESCE(?, expire_date),
                refresh_token = COALESCE(?, refresh_token),
                img_url = COALESCE(?, img_url),
                active = COALESCE(?, active)
            WHERE id = ?`;
        var result = await db.query(sql, [profile.platformUserId, profile.contentMasterId, profile.userId, profile.userName, profile.displayName, profile.fullName, profile.email,
        profile.emailVerified, profile.platform, profile.accessToken, profile.expireDate?.toSqlFormat(), profile.refreshToken, profile.imgUrl, profile.active, profile.id]);
        if(toView) profile = profile.toView();
        return profile;
        //return true;
    },

    /**
     * 
     * @param {Number} profileId 
     */
    delete: async (profileId) => {
        let sql = 'UPDATE profiles SET active = FALSE WHERE id = ?';
        var result = await db.query(sql, profileId);
        return true;
    },

    /**
     * 
     * @param {Profile} profile 
     * @returns {Boolean}
     */
    exist: async (profile) => {
        let sql = `SELECT EXISTS(SELECT * FROM profiles WHERE 
            (id=?) OR
            (platform_user_id=? AND platform=?) OR
            (user_name=? AND platform=?)
            LIMIT 1
            ) AS exist`;
        var result = await db.query(sql, [profile.id, profile.platformUserId, profile.platform, profile.userId, profile.platform]);
        return !!result[0].exist; //!! convert object to boolean (0, null or undefined is FALSE)
    },

}