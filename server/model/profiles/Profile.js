const Joi = require('joi');
const BaseObject = require('../BaseObject');
const { PLATFORMS_LOGINABLE } = require('../enums');
const User = require('../users/User');

class Profile extends BaseObject {

    constructor(data) {
        super(); //call ancestor constructor
        if (data) {
            this.id = this.platformUserId = this.userId = this.userName = this.contentMasterId = this.displayName = this.fullName = this.email =
                this.emailVerified = this.platform = this.contentSyncDate = this.imgUrl = this.url = this.accessToken = this.expireDate = this.refreshToken =
                this.createDate = this.updateDate = this.active = undefined;
            Object.assign(this, data);
        }
    }

    isValid() {
        return validSchema.validate(this);
    }

    get isLoginable(){
        return PLATFORMS_LOGINABLE.hasOwnProperty(this.platform);
    }

    static columnsToView = 'id, user_id, user_name, display_name, platform, active';

    toView() {
        return new Profile({
            id: this.id,
            userId: this.userId,
            displayName: this.displayName,
            platform: this.platform,
            active: this.active
        })
    }

    /**
     * create new user instance filled by profile data
     * 
     * @returns new user
     */
    toUser() {
        return new User({
            id: this.userId,
            displayName: this.displayName,
            fullName: this.fullName,
            email: this.email,
            emailVerified: this.emailVerified,
            imgUrl: this.imgUrl
        });
    }
}

const validSchema = Joi.object({
    id:
        Joi.number().integer().positive().max(99999999999)
            .allow(null).optional(), //allow(null) => null.. optional() => undefined
    platformUserId:
        Joi.string()
            .required(),
    userId:
        Joi.string()
            .allow(null).optional(),
    contentMasterId:
        Joi.number().integer().positive().max(99999999999)
            .allow(null).optional(),
    userName:
        Joi.string()
            .allow(null).optional(),
    displayName:
        Joi.string()
            .required(),
    fullName:
        Joi.string()
            .allow(null).optional(),
    email:
        Joi.string().email()
            .allow(null).optional(),
    emailVerified:
        Joi.boolean().falsy('false').truthy('true')
            .allow(null).optional(),
    platform:
        Joi.string()
            .required(),
    contentSyncDate:
        Joi.date()
            .allow(null).optional(),
    url:
        Joi.string()
            .allow(null).optional(),
    imgUrl:
        Joi.string()
            .allow(null).optional(),
    accessToken:
        Joi.string()
            .allow(null).optional(),
    expireDate:
        Joi.date()
            .allow(null).optional(),
    refreshToken:
        Joi.string()
            .allow(null).optional(),
    createDate:
        Joi.date()
            .allow(null).optional(),
    updateDate:
        Joi.date()
            .allow(null).optional(),
    active:
        Joi.bool()
            .allow(null).optional(),
});

module.exports = Profile;