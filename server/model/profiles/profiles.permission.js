const { ROLE } = require('../../model/enums')
const User = require('../users/User')
const Profile = require('./Profile')

/**
 * 
 * @param {User} currentUser 
 * @param {Profile} profileToView 
 * @returns 
 */
function canView(currentUser, profileToView) {
    return (
        currentUser.role === ROLE.ADMIN ||
        profileToView.userId === currentUser.id
    );
}

function canUpdate(currentUser, profileToUpdate) {
    return canView(currentUser, profileToUpdate);
}

function canDelete(currentUser, profileToDelete) {
    return canView(currentUser, profileToDelete);
}

///filter users that not allowed to show to current user
function filterList(currentUser, profilesToView) {
    if (!profilesToView || currentUser.role === ROLE.ADMIN) return profilesToView;
    return profilesToView.filter(profile => profile.userId === currentUser.id);
}

module.exports = {
    canView,
    canUpdate,
    canDelete,
    filterList
}