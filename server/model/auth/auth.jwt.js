var jwt = require('jsonwebtoken');
const { HttpStatusCode } = require('../../errors/error.enums');
const UserError = require('../../errors/UserError');
const UserService = require('../users/users.service');

function authMiddleware(req, res, next) {
    getCurrentUserFromCookie(req)
        .then(user => {
            if (user != null && !user.isEmpty()) {
                req.user = user;
                next();
            }
            else {
                res.status(401).send('Unauthorized');
            }
        })
        .catch(err => {
            next(err);
        })
}

async function getCurrentUserFromCookie(req) {
    var currentUser = null;
    try {
        var token = extractJwtFromCookie(req);
        if (token) {
            var decoded = jwt.verify(token, process.env.JWT_SECRET);

            var user = await UserService.findById(decoded.sub)
            if (!user.isEmpty()) {
                currentUser = user;
            }
        }
    } catch (err) {
        throw new UserError("Authorization token is not valid!", HttpStatusCode.UNAUTHORIZED);
    }
    return currentUser;
}

function extractJwtFromCookie(req) {
    var token = null;
    if (req && req.cookies)
        token = req.cookies['jwt'];
    return token;
};

function authRole(role) {
    return (req, res, next) => {
        if (req.user.role !== role) {
            res.status(403);
            return res.send('Forbidden');
        }
        next();
    }
}

function jwtFromUser(user){
    return jwt.sign({ sub: user.id, name: user.displayName, role: user.role }, process.env.JWT_SECRET);
}

module.exports = {
    auth: authMiddleware,
    getCurrentUserFromCookie,
    roleUser: authRole('USER'),
    roleAdmin: authRole('ADMIN'),
    jwtFromUser
}
