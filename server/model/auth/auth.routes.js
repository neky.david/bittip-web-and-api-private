const express = require('express');
const asyncHandler = require('express-async-handler')
const router = express.Router();
const bcrypt = require('bcryptjs');
const authLocal = require('./auth.local')
const authJwt = require('./auth.jwt');
// Load User model
const authService = require('./auth.service');

// Register
router.post('/register', asyncHandler(async (req, res) => {
  const { displayName, email, password, password2 } = req.body;
  var newUser = await authService.register(displayName, email, password, password2);

  //JWT
  var jwtToken = authJwt.jwtFromUser(newUser);
  res.cookie('jwt', jwtToken, {
    maxAge: 86400000,
    httpOnly: true,
    secure: process.env.NODE_ENV === 'production' ? true : false
    //domain: 'localhost'
  });

  res.send(newUser);
}));

// Login
router.post('/login', authLocal({
  session: false
}), (req, res) => {

  //JWT
  var jwtToken = authJwt.jwtFromUser(req.user);
  res.cookie('jwt', jwtToken, {
    maxAge: 86400000,
    httpOnly: true,
    secure: process.env.NODE_ENV === 'production' ? true : false
    //domain: 'localhost'
  });

  res.send(req.user);
});

// Logout
router.post('/logout', (req, res) => {
  res.cookie('jwt', '', { maxAge: 1 });
  res.send();
})

module.exports = router;
