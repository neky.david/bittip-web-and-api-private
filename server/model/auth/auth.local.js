const LocalStrategy = require('passport-local').Strategy;
const passport = require('passport');
const bcrypt = require('bcryptjs');
const UserError = require('../../errors/UserError')
const { HttpStatusCode } = require('../../errors/error.enums');

// Load User model
const userService = require('../users/users.service');

passport.use(new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
  // Match user
  userService.findByEmail(email, false)
    .then(user => {
      if (user.isEmpty() || !user.password) {
        return done(new UserError('Password or email is incorrect', HttpStatusCode.BAD_REQUEST), false);
      }

      // Match password
      bcrypt.compare(password, user.password, (err, isMatch) => {
        if (err) throw err;
        if (isMatch) {
          return done(null, user);
        } else {
          return done(new UserError('Password or email is incorrect', HttpStatusCode.BAD_REQUEST), false);
        }
      });
    });
}));

module.exports = (options) => {
  return passport.authenticate('local', options);
}