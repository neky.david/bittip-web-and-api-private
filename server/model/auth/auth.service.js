const userService = require('../users/users.service');
const UserError = require('../../errors/UserError');
const { HttpStatusCode } = require('../../errors/error.enums');
const Joi = require('joi');
const passport = require('passport');
var bcrypt = require('bcryptjs');
const User = require('../users/User');

async function register(displayName, email, password, password2) {

    if (!displayName || !email || !password || !password2) {
        throw new UserError('Please enter all fields', HttpStatusCode.BAD_REQUEST);
    }
    if (password != password2) {
        throw new UserError('Passwords do not match', HttpStatusCode.BAD_REQUEST);
    }
    if (!isPasswordValid(password)) {
        throw new UserError('Password must be at least 8 characters and contains one lowercase and uppercase character and one number.', HttpStatusCode.BAD_REQUEST);
    }

    var user = await userService.findByEmail(email);
    if (!user.isEmpty()) {
        throw new UserError('User with this email already exists', HttpStatusCode.BAD_REQUEST);
    } else {
        const newUser = new User({
            displayName,
            email,
            password
        });

        var salt = bcrypt.genSaltSync(10);
        var hash = bcrypt.hashSync(newUser.password, salt);

        newUser.password = hash;
        return await userService.create(newUser);
    }
};

function isPasswordValid(password) {
    var v = Joi.string()
        .pattern(new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})"))
        .validate(password)
    return v.error ? false : true;
}

module.exports = {
    register
};