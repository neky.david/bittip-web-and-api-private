require('dotenv').config();
const mysql = require('mysql');
const util = require('util');
const naming = require('../helpers/naming.helper');
let count = 0;

var dbConnection = null;

//Connect to db
const connectWithRetry = () => {
    //Create db connection
    dbConnection = mysql.createConnection({
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME
    });

    dbConnection.connect((err) => {
        if (err) {
            //console.error(err);
            console.log('MySql connection unsuccessful, retry after 5 seconds. ', ++count);
            setTimeout(connectWithRetry, 5000)
        } else {
            console.log('MySql succesfully connected..');
        }
    });
}

connectWithRetry();


module.exports = {
    query: async (sql, args, options) => {
        args = naming.objectNamesToUnderscore(args);

        //loging
        //var rows = [];
        // if (sql.startsWith('UPDATE'))
        //console.log(dbConnection.query(sql, args).sql);

        // console.log(' ');
        // console.log('SQL - ' + sql);
        // console.log('ARGS: ');
        // console.log(args);
        // console.log('ROWS:');

        var rows = await util.promisify(dbConnection.query).call(dbConnection, sql, args);

        //console.log(rows);

        var results = [];

        if (rows) {
            // Object.keys(rows).forEach(function (key) {
            //     results.push(rows[key]);
            // });
            if (rows.length > 0) {
                await rows.forEach(row => {
                    results.push(row);
                });
            }
            else {
                results = rows;
            }

            if (results)
                results = naming.objectNamesToCamel(results);
        }

        if (options?.firstOrNull) {
            return results.length > 0 ? results[0] : null;
        }
        return results;
    },
    close: () => {
        return util.promisify(dbConnection.end).call(dbConnection);
    },
    isConnected: () => {
        return dbConnection.state === 'connected' || dbConnection.state === 'authenticated'
    },
    beginTransaction: () => {
        return util.promisify(dbConnection.beginTransaction).call(dbConnection);
    },
    commitTransaction: () => {
        return util.promisify(dbConnection.commit).call(dbConnection);
    },
    rollbackTransaction: () => {
        return util.promisify(dbConnection.rollback).call(dbConnection);
    }
};

/* SAMPLES
//Create DB
app.get('/createdb', (req, res) => {
    let sql = 'CREATE DATABASE bittip';
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('Database created..')
    });
})

//Create table
app.get('/createtable', (req, res) => {
    let sql = 'CREATE TABLE tests(id int AUTO_INCREMENT, title VARCHAR(255), body VARCHAR(255), PRIMARY KEY(id))';
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('Table TEST created..');
    });
})

//INSERT data
app.get('/insert', (req, res) => {
    let test = {
        title: 'Test1',
        body: 'Body1'
    }
    let sql = 'INSERT INTO tests SET ?';
    db.query(sql, test, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('Data inserted..');
    });
})

//INSERT data 2
app.get('/insert2', (req, res) => {
    let test = {
        title: 'Test2',
        body: 'Body2'
    }
    let sql = 'INSERT INTO tests SET ?';
    db.query(sql, test, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('Data 2 inserted..');
    });
})

//SELECT
app.get('/select', (req, res) => {
    let sql = 'SELECT * FROM tests';
    db.query(sql, (err, results) => {
        if(err) throw err;
        console.log(results);
        res.send('SELECTED..');
    });
})

//SELECT single
app.get('/select/:id', (req, res) => {
    let sql = `SELECT * FROM tests WHERE id = ${req.params.id}`;
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('SELECTED..');
    });
})

//UPDATE
app.get('/update/:id', (req, res) => {
    let newTitle = 'Updated Title';
    let sql = `UPDATE tests SET title = '${newTitle}' WHERE id = ${req.params.id}`;
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('Updated..');
    });
})

//UPDATE
app.get('/delete/:id', (req, res) => {
    let sql = `DELETE FROM tests WHERE id = ${req.params.id}`;
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send('DELETED..');
    });
})
*/