const lnurl = require('lnurl');
var path = require("path");

const server = lnurl.createServer({
	host: 'localhost',
	port: process.env.LNURL_PORT || 9437,
	url: process.env.LNURL_SERVER_URL,
	auth: {
		apiKeys: [
			{
				id: process.env.LNDURL_API_KEY_ID,
				key: process.env.LNDURL_API_KEY,
				encoding: 'hex',
			},
		],
	},
	store: {
		backend: 'knex',
		config: {
			client: 'mysql',
			connection: {
				host: process.env.DB_HOST,
				user: process.env.DB_USER,
				password: process.env.DB_PASS,
				database: process.env.DB_NAME,
			},
		},
	},
	lightning: {
		backend: {
			path: path.resolve('./services/lightning/lnurl.backend.js'), //'G:/Projects/BitTip/bittip-web-and-api-private/server/services/lightning/lnurl.backend.js'
		},
	},
});

server.bindToHook('withdrawRequest:action', async function (secret, params, next) {

	const paymentService = require('../../model/payments/payments.service');
	const walletService = require('../../model/wallets/wallets.service');

	if (process.env.TESTING) {
		console.log('LNURL HOOK action ****************************************');
		console.log(secret);
		console.log(params);
		console.log(next);
	}

	if (secret && params.maxWithdrawable && params.pr) {

		var payment = await paymentService.findOutgoingByLnurlSecret(secret, true);
		if (payment.isEmpty()) throw new Error();

		if (process.env.TESTING) {
			console.log('Payment');
			console.log(payment);
		}

		var wallet = await walletService.findById(payment.walletId);

		if (wallet.isEmpty()) throw new Error();

		if (process.env.TESTING) {
			console.log('Wallet');
			console.log(wallet);
		}

		if (wallet.amount < params.maxWithdrawable - parseInt(process.env.WITHDRAW_FEE_RESERVE_MSATS || 10000)) throw Error();
		//TODO: pokud bude maxWithdrawable hodně velký tak provézt kontrolu přes SQL že uživatel opravdu může vybrat tolik

		var payRequests = params.pr.split(',');

		if (process.env.TESTING) {
			console.log('pay requests');
			console.log(payRequests);
		}
		await paymentService.makeCopyForEachPayRequest(payment, payRequests);
	}
});

console.log("LNURL server running");

module.exports = server;