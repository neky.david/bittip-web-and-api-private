const lightning = require('./lightning.service');
const invoiceTracker = require('./invoice.tracker');
const lnurl = require('./lnurl.service');

async function test() {
    try {

        var res = null;

        //TEST
        //lightning.test();

        //INFO
        //res = await lightning.getInfo();

        //ADD INVOICE & CHECK INVOICE
        //res = await lightning.addInvoice(0, 'TESTING ZERO INVOICE');
        // console.log(res);
        //var payReq = res.payment_request;
        //res = await lightning.checkInvoice(payReq);
        // console.log(res);

        //CHECK INVOICE & PAY INVOICE
        // var payReq = 'lnbc10u1pssyn5lpp5yjvpjj8awzhgwj3klckq53c02vs3raldafn5jq9awvesrjk8xjyqdqqcqzpgxqyz5vqsp5df4p6ggd6l09je5unzs5e460xuq9m90lda3kkn4ak4hj409qfwnq9qyyssqluddtga4fsdkrn4cl0aq07k2hlr0vd8hfw5rr3f8hqy0ye2za2s3llmy9whp347vdxhftgtp5d3kthru59m62adt7s585wec7zl3x5qq7j6vtq';
        // res = await lightning.checkInvoice(payReq);
        // console.log(res);

        // var ResJson = JSON.stringify(res);
        // console.log(varResJson);

        // if (res.num_satoshis < 5000) {
        //     //res = await lightning.sendPayment(payReq);
        //     res = await lightning.sendPaymentSync(payReq);
        // }
        // else{
        //     console.log(`number of satoshi not valid - ${res.num_satoshis}`);
        // }

        //ADD INVOICE & CHECK PAYMENT
        //res = await lightning.checkPayment("CRvX6+s1ZB6sDh2rU0ORs2Isu7NcUbgAEF8x/I83Jro="); //res.r_hash.toString('base64').. res.amt_paid_sat


        //ESTIMATE FEE --not working
        // var payReq = 'lnbc2510n1ps0zehqpp5hdmgedefgl0ftxe7p2vd78330xs8n9fxe7g4ma4c7msxuq90lwqqdqqcqzpgxqyz5vqsp5wwpa6c0p62m7mnvq9t692nu3mnws9daakj6dt0vrt6a5uqw04ycq9qyyssqc2c697l7erujzjdfmxchjyjs46282jqnuyulpvjxrngjxfc9vwzzpnxk3qr9amxfsnaqkjrmwvcvrc7p878fc505utvqn7g9rcfm8aqqeqq8ah';
        // res = await lightning.checkInvoice(payReq);
        // console.log(res);
        // res = await lightning.estimateRouteFee(res.destination, res.num_satoshis);
        // //res = await lightning.estimateRouteFee("03abf6f44c355dec0d5aa155bdbdd6e0c8fefe318eff402de65c6eb2e1be55dc3e@18.221.23.28:9735", res.num_satoshis);
        // //res = await lightning.estimateRouteFee("03abf6f44c355dec0d5aa155bdbdd6e0c8fefe318eff402de65c6eb2e1be55dc3e", res.num_satoshis);

        //TRACK INVOICES
        res = await lightning.addInvoice(0, 'TESTING ZERO INVOICE');
        console.log(res);

        res = await invoiceTracker.waitToSettle(res.add_index);

        console.log(res);
        // var json = JSON.stringify(res);
        // console.log(json);
    }
    catch (err) {
        console.log('ERROR:');
        console.log(err);
        var json = JSON.stringify(err);
        console.log(json);
    }
}

async function testLnurl(){
    //API KEY GENERATING
    // const { id, key, encoding } = lnurl.generateApiKey();
    // console.log({ id, key, encoding });

    //WITHDRAW URL
    var result = await lnurl.withdraw(10, 1000, "TEST DESCRIPTION");
    console.log(result);
}

//test();

testLnurl();