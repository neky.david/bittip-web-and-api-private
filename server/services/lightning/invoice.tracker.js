const { lnd } = require('./lnd');
const paymentService = require('../../model/payments/payments.service')
const naming = require('../../helpers/naming.helper');
const Payment = require('../../model/payments/Payment');
const dateExt = require('../../helpers/date.ext');

var call = null;

var onSettledFuncs = []

function runTracker(addIndex) {
    try {

        if (call == null || call.destroyed) {
            let request = {
                add_index: (addIndex || 1) - 1
            };

            call = lnd.subscribeInvoices(request);
            call.on('data', onData);
            //call.on('status', onStatus);
            call.on('end', onEnd);
            call.on('error', onError);
            console.log('INVOICE Tracker is running');
        }
    }
    catch (err) {
        call = null;
        throw err;
    }
}

function stopTracker() {
    if (call && call.destroyed) {
        try {
            call.cancel();
            call.destroy();
        }
        catch (err) {
            console.log(err);
        }
    }
    //call = null;
    console.log('INVOICE Tracker STOPED');
}

function restartTracker() {
    stopTracker();
    runTracker();
}

async function onData(data) {
    if (process.env.TESTING) {
        console.log('subscribeInvoices - DATA');
        console.log(data);
    }

    if (data.state === 'SETTLED' && data.settled) {

        //update payment
        data = naming.objectNamesToCamel(data);
        var payment = await paymentService.updatePaymentFromSettledInvoice(data);

        //call waiting func
        var hash = data.rHash.toString('base64');
        var item = onSettledFuncs.find(i => i.hash == hash);
        if (item && item.onSuccess) {
            try {
                item.onSuccess(payment);
            }
            catch (err) {
                console.log(err);
            }
            removeSettledCallback(item.hash);
        }
    }
}

// function onStatus(status) {
//     console.log('subscribeInvoices - STATUS:');
//     console.log(status);
// }

function onEnd() {
    console.log('subscribeInvoices - END:');
    //restartTracker()
}

function onError(err) {
    console.log('INVOICE Tracker has ERROR');
    console.log(err);
    if (err.code != 14) //No connection established
        restartTracker();
}

function addSettledCallback(hash, onSuccess, onError) {
    //runTracker(hash);

    onSettledFuncs.push({
        hash: hash,
        onSuccess: onSuccess,
        onError: onError,
        addDate: new Date()
    })
}

function removeSettledCallback(hash) {
    var hourBefore = new Date().addHours(-1);
    onSettledFuncs = onSettledFuncs.filter(i => i.hash != hash && i.addDate > hourBefore);
    // if (onSettledFuncs.length === 0)
    //     stopTracker();
}

module.exports = {

    run: runTracker,

    // waitOnAllSettled: (callback) => {},

    /**
     * Func waiting to settle invoice and then returns updated payment
     * 
     * @param {String} hash 
     * @returns {Payment} updated payment
     */
    waitToSettle: (hash) => {
        return new Promise((resolve, reject) => {
            try {
                addSettledCallback(hash, resolve, reject);
            }
            catch (err) {
                reject(err);
            }
        });
    }

}