const { lnd, router } = require('./lnd');
const mockObjects = require('./mock.objects');
const naming = require('../../helpers/naming.helper');

module.exports = {

  /**
   * GetInfo returns general information concerning the lightning node including it's identity pubkey, alias, the chains it is connected to, and information concerning the number of open+pending channels.
  * @returns general information concerning the lightning node 
  */
  getInfo: () => {
    return new Promise((resolve, reject) => {

      lnd.getInfo({}, function (err, response) {
        if (err) reject(err);
        resolve(naming.objectNamesToCamel(response));
      });

    });
  },

  /**
   * AddInvoice attempts to add a new invoice to the invoice database. Any duplicated invoices are rejected, therefore all invoices must have a unique payment preimage.
   * 
   * @param {Number} value The value of this invoice in satoshis The fields value and value_msat are mutually exclusive.
   * @param {String} memo An optional memo to attach along with the invoice. Used for record keeping purposes for the invoice's creator, and will also be set in the description field of the encoded payment request if the description_hash field is not being used.
   * @param {Number} expiry Payment request expiry time in seconds. Default is 86400 (1 day).
   * @returns new invoice with payment request attribute
   */
  addInvoice: (value, memo, expiry) => {
    return new Promise((resolve, reject) => {

      if (process.env.LIGHTNING_API_MOCK === 'true') resolve(mockObjects.AddInvoiceResponse);

      let request = {
        value: value,
        memo: memo,
        expiry: expiry //default is 86400 (1 day)
      };
      lnd.addInvoice(request, function (err, response) {
        if (err) reject(err);
        response.payment_addr = response.payment_addr.toString('base64');
        response.r_hash = response.r_hash.toString('base64');
        resolve(naming.objectNamesToCamel(response));
      });

    });
  },

  /**
   * DecodePayReq takes an encoded payment request string and attempts to decode it, returning a full description of the conditions encoded within the payment request.
   * 
   * @param {String} paymentRequest The payment request string to be decoded
   * @returns full description of the conditions encoded within the payment request
   */
  checkInvoice: (paymentRequest) => {
    return new Promise((resolve, reject) => {

      if (process.env.LIGHTNING_API_MOCK === 'true') resolve(mockObjects.CheckInvoiceResponse);

      let request = {
        pay_req: paymentRequest
      };
      lnd.decodePayReq(request, function (err, response) {
        if (err) reject(err);
        resolve(naming.objectNamesToCamel(response));
      });

    });
  },

  /**
   * SendPaymentSync is the synchronous non-streaming version of SendPayment. This RPC is intended to be consumed by clients of the REST proxy. Additionally, this RPC expects the destination's public key and the payment hash (if any) to be encoded as hex strings.
   * @param {String} paymentRequest
   */
  sendPaymentSync: (paymentRequest, amt, feeLimit = 10) => {
    return new Promise((resolve, reject) => {

      if (process.env.LIGHTNING_API_MOCK === 'true') resolve(mockObjects.PaymentResponse);

      let request = {
        payment_request: paymentRequest,
        amt: amt,
        fee_limit: { fixed: feeLimit }
      };

      lnd.sendPaymentSync(request, function (err, response) {
        if (err) reject(err);
        resolve(naming.objectNamesToCamel(response));
      });

    });
  },

  /**
   * LookupInvoice attempts to look up an invoice according to its payment hash. The passed payment hash must be exactly 32 bytes, if not, an error is returned.
   * 
   * @param {String} rHash The payment hash of the invoice to be looked up. When using REST, this field must be encoded as base64.
   * @returns 
   */
  checkPayment: (rHash) => {
    return new Promise((resolve, reject) => {

      if (process.env.LIGHTNING_API_MOCK === 'true') resolve(mockObjects.CheckPaymentResponse);

      let request = {
        r_hash: rHash
      };
      lnd.lookupInvoice(request, function (err, response) {
        if (err) reject(err);
        resolve(naming.objectNamesToCamel(response));
      });

    });
  },


  // /**
  //  * EstimateRouteFee allows callers to obtain a lower bound w.r.t how much it may cost to send an HTLC to the target end destination.
  //  * 
  //  * @param {String} destination The destination once wishes to obtain a routing fee quote to. The identity pubkey of the payment recipient.
  //  * @param {Number} amount The amount one wishes to send to the target destination.
  //  * @returns 
  //  */
  // estimateRouteFee: (destination, amount) => {
  //   return new Promise((resolve, reject) => {

  //     var enc = new TextEncoder(); // always utf-8
  //     console.log(destination);
  //     console.log(enc.encode(destination));
  //     console.log(Buffer.from(destination, "utf-8"));
  //     console.log(Buffer.from(destination).toString('base64'));

  //     let request = {
  //       dest: destination, //Buffer.from(destination).toString('base64'), //Buffer.from(destination, "utf-8") //enc.encode(destination),
  //       amt_sat: amount
  //     };
  //     router.estimateRouteFee(request, function (err, response) {
  //       if (err) reject(err);
  //       resolve(response);
  //     });

  //   });
  // },

  // /**
  //  * SendPaymentV2 attempts to route a payment described by the passed PaymentRequest to the final destination. The call returns a stream of payment updates.
  //  * @param {String} paymentRequest
  //  */
  //  sendPayment: (paymentRequest, feeLimit = 10, timeoutSeconds = 10) => {
  //   return new Promise((resolve, reject) => {

  //     try {
  //       let request = {
  //         pay_req: paymentRequest,
  //         timeout_seconds: timeoutSeconds,
  //         fee_limit_sat: feeLimit
  //       };

  //       let call = router.sendPaymentV2(request);
  //       call.on('data', function (response) {
  //         // A response was received from the server.
  //         console.log(response);
  //         resolve(response);
  //       });
  //       call.on('status', function (status) {
  //         // The current status of the stream.
  //         console.log(status);
  //       });
  //       call.on('end', function () {
  //         // The server has closed the stream.
  //         console.log('END');
  //       });
  //     }
  //     catch (err) {
  //       reject(err);
  //     }

  //   });
  // },

  test: () => {
    let request = {};
    router.getMissionControlConfig(request, function (err, response) {
      console.log(response);
    });
  }

}