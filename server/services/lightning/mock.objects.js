
module.exports = {
    AddInvoiceResponse: {
        "r_hash": "r_hashTestTestTestTestTestTestTestTestTest",
        "payment_request": "lnbc1ps0rx9fpp5pyda06ltx4jpatqwrk44xsu3kd3zewant3gmsqqstuclerehy6aqdpq23z4x4zfferjqkj92f8jqj2w2e85js69cqzpgxqyz5vqsp5jh278g90zf8ktuxkhdtc3yljr92yvy3t8jrs88qm52k3fcwc53mq9qyyssqc3d8ypv2kkhuf4qr64yr3r8869nckn3hp38lakkym85j4nqsktw36u6n2cdqwzyezm3w9j9pu35kzlycktsax8v5wm9k2erkvve7gusp03pwyj",
        "add_index": 123456,
        "payment_addr": "payment_addrTestTestTestTestTestTestTestTestTest"
    },
    CheckInvoiceResponse: {
        "route_hints": [],
        "features":
        {
            "9":
            {
                "name": "tlv-onion",
                "is_required": false,
                "is_known": true
            },
            "14":
            {
                "name": "payment-addr",
                "is_required": true,
                "is_known": true
            },
            "17":
            {
                "name": "multi-path-payments",
                "is_required": false,
                "is_known": true
            }
        },
        "destination": "028ffdb448b1a54ec3b8f6adc46c842475ea03626ec5d35df6e0343fca214be8b2",
        "payment_hash": "091bd7ebeb35641eac0e1dab534391b3622cbbb35c51b800105f31fc8f3726ba",
        "num_satoshis": "0",
        "timestamp": "1626445993",
        "expiry": "86400",
        "description": "TESTING ZERO INVOICE",
        "description_hash": "",
        "fallback_addr": "",
        "cltv_expiry": "40",
        "payment_addr":
        {
            "type": "Buffer",
            "data": [149, 213, 227, 160, 175, 18, 79, 101, 240, 214, 187, 87, 136, 147, 242, 25, 84, 70, 18, 43, 60, 135, 3, 156, 27, 162, 173, 20, 225, 216, 164, 118]
        },
        "num_msat": "0"
    },
    CheckPaymentResponse: {
        "route_hints": [],
        "htlcs": [
            {
                "custom_records": {},
                "chan_id": "759912068482072577",
                "htlc_index": "3",
                "amt_msat": "56000",
                "accept_height": 691290,
                "accept_time": "1626446109",
                "resolve_time": "1626446109",
                "expiry_height": 691333,
                "state": "SETTLED",
                "mpp_total_amt_msat": "56000",
                "amp": null
            }
        ],
        "features":
        {
            "9":
            {
                "name": "tlv-onion",
                "is_required": false,
                "is_known": true
            },
            "14":
            {
                "name": "payment-addr",
                "is_required": true,
                "is_known": true
            },
            "17":
            {
                "name": "multi-path-payments",
                "is_required": false,
                "is_known": true
            }
        },
        "memo": "TESTING ZERO INVOICE",
        "r_preimage":
        {
            "type": "Buffer",
            "data": [48, 184, 139, 51, 160, 113, 162, 58, 103, 166, 80, 144, 65, 50, 116, 228, 95, 30, 211, 13, 74, 84, 190, 18, 176, 77, 98, 47, 25, 55, 159, 23]
        },
        "r_hash":
        {
            "type": "Buffer",
            "data": [9, 27, 215, 235, 235, 53, 100, 30, 172, 14, 29, 171, 83, 67, 145, 179, 98, 44, 187, 179, 92, 81, 184, 0, 16, 95, 49, 252, 143, 55, 38, 186]
        },
        "value": "0",
        "settled": true,
        "creation_date": "1626445993",
        "settle_date": "1626446109",
        "payment_request": "lnbc1ps0rx9fpp5pyda06ltx4jpatqwrk44xsu3kd3zewant3gmsqqstuclerehy6aqdpq23z4x4zfferjqkj92f8jqj2w2e85js69cqzpgxqyz5vqsp5jh278g90zf8ktuxkhdtc3yljr92yvy3t8jrs88qm52k3fcwc53mq9qyyssqc3d8ypv2kkhuf4qr64yr3r8869nckn3hp38lakkym85j4nqsktw36u6n2cdqwzyezm3w9j9pu35kzlycktsax8v5wm9k2erkvve7gusp03pwyj",
        "description_hash":
        {
            "type": "Buffer",
            "data": []
        },
        "expiry": "86400",
        "fallback_addr": "",
        "cltv_expiry": "40",
        "private": false,
        "add_index": "11",
        "settle_index": "5",
        "amt_paid": "56000",
        "amt_paid_sat": "56",
        "amt_paid_msat": "56000",
        "state": "SETTLED",
        "value_msat": "0",
        "is_keysend": false,
        "payment_addr":
        {
            "type": "Buffer",
            "data": [149, 213, 227, 160, 175, 18, 79, 101, 240, 214, 187, 87, 136, 147, 242, 25, 84, 70, 18, 43, 60, 135, 3, 156, 27, 162, 173, 20, 225, 216, 164, 118]
        },
        "is_amp": false
    },
    PaymentResponse: {
        "payment_error": "",
        "payment_preimage":
        {
            "type": "Buffer",
            "data": [246, 224, 147, 87, 173, 200, 157, 204, 211, 66, 114, 34, 9, 95, 195, 141, 47, 84, 208, 109, 191, 217, 236, 125, 111, 159, 219, 116, 250, 30, 48, 253]
        },
        "payment_route":
        {
            "hops": [
                {
                    "custom_records": {},
                    "chan_id": "758091277174702080",
                    "chan_capacity": "60000",
                    "amt_to_forward": "1001",
                    "fee": "0",
                    "expiry": 693367,
                    "amt_to_forward_msat": "1001100",
                    "fee_msat": "176",
                    "pub_key": "030995c0c0217d763c2274aa6ed69a0bb85fa2f7d118f93631550f3b6219a577f5",
                    "tlv_payload": true,
                    "mpp_record": null,
                    "amp_record": null
                },
                {
                    "custom_records": {},
                    "chan_id": "622281799956889600",
                    "chan_capacity": "500000",
                    "amt_to_forward": "1000",
                    "fee": "1",
                    "expiry": 693223,
                    "amt_to_forward_msat": "1000000",
                    "fee_msat": "1100",
                    "pub_key": "03864ef025fde8fb587d989186ce6a4a186895ee44a926bfc370e2c366597a3f8f",
                    "tlv_payload": true,
                    "mpp_record": null,
                    "amp_record": null
                },
                {
                    "custom_records": {},
                    "chan_id": "759773529967689729",
                    "chan_capacity": "100000000",
                    "amt_to_forward": "1000",
                    "fee": "0",
                    "expiry": 693223,
                    "amt_to_forward_msat": "1000000",
                    "fee_msat": "0",
                    "pub_key": "037cc5f9f1da20ac0d60e83989729a204a33cc2d8e80438969fadf35c1c5f1233b",
                    "tlv_payload": true,
                    "mpp_record":
                    {
                        "total_amt_msat": "1000000",
                        "payment_addr":
                        {
                            "type": "Buffer",
                            "data": [106, 106, 29, 33, 13, 215, 222, 89, 102, 156, 152, 161, 76, 215, 79, 55, 0, 93, 149, 255, 111, 99, 107, 78, 189, 181, 111, 42, 188, 160, 75, 166]
                        }
                    },
                    "amp_record": null
                }],
            "total_time_lock": 693407,
            "total_fees": "1",
            "total_amt": "1001",
            "total_fees_msat": "1276",
            "total_amt_msat": "1001276"
        },
        "payment_hash":
        {
            "type": "Buffer",
            "data": [36, 152, 25, 72, 253, 112, 174, 135, 74, 54, 254, 44, 10, 71, 15, 83, 33, 17, 247, 237, 234, 103, 73, 0, 189, 115, 51, 1, 202, 199, 52, 136]
        }
    },
    PaymentResponseError: {
        "code": 2,
        "details": "invoice expired. Valid until 2021-07-17 09:33:13 -0500 CDT",
        "metadata":
        {
            "internalRepr": {},
            "options": {}
        }
    }

}