require('dotenv').config();
const fs = require('fs');
//const grpc = require('grpc');
const grpc = require('@grpc/grpc-js');
const protoLoader = require('@grpc/proto-loader');

// We need to give the proto loader some extra options, otherwise the code won't
// fully work with lnd.
const loaderOptions = {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true
};

const prePath = process.env.LIGHTNING_API_LOCAL ? "" : "services/lightning/";

//load .proto file
const packageDefinition = protoLoader.loadSync([prePath + 'rpc.proto', prePath + 'router.proto'], loaderOptions);
const lnrpc = grpc.loadPackageDefinition(packageDefinition).lnrpc;
const routerrpc = grpc.loadPackageDefinition(packageDefinition).routerrpc;

// Due to updated ECDSA generated tls.cert we need to let gprc know that
// we need to use that cipher suite otherwise there will be a handhsake
// error when we communicate with the lnd rpc server.
process.env.GRPC_SSL_CIPHER_SUITES = 'HIGH+ECDSA';

// load LND cert
const lndCert = fs.readFileSync(prePath + 'tls.cert');
const sslCreds = grpc.credentials.createSsl(lndCert);

// load MACAROON cred
const macaroon = fs.readFileSync(prePath + "admin.macaroon").toString('hex');
const macaroonCreds = grpc.credentials.createFromMetadataGenerator(function(args, callback) {
  let metadata = new grpc.Metadata();
  metadata.add('macaroon', macaroon);
  callback(null, metadata);
});

let creds = grpc.credentials.combineChannelCredentials(sslCreds, macaroonCreds);
let lightning = new lnrpc.Lightning(process.env.LIGHTNING_API_URL, creds);
let router = new routerrpc.Router(process.env.LIGHTNING_API_URL, creds);

module.exports = {
  lnd: lightning,
  router: router
};