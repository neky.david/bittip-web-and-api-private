const lnurl = require('lnurl');
const server = require('./lnurl.server');

const withdrawTag = 'withdrawRequest';

module.exports = {

    /**
     * 
     * @param {Number} minWithdrawable 
     * @param {Number} maxWithdrawable
     * @param {String} defaultDescription 
     * @returns {{ encoded, secret, url }} { encoded, secret, url }
     */
    withdraw: async (minWithdrawable, maxWithdrawable, defaultDescription) => {

        const params = {
            minWithdrawable,
            maxWithdrawable,
            defaultDescription
        };

        // const options = {
        //     uses: 3,
        // };

        var result = await server.generateNewUrl(withdrawTag, params);
        if (process.env.TESTING) {
            console.log("LNURL Service Withdraw");
            console.log(result);
        }
        return result;
    },

    generateApiKey: async () => {
        return await lnurl.generateApiKey();
    }

}

// signedWithdraw: async (minWithdrawable, maxWithdrawable, defaultDescription) => {

//     var apiKey = lnurl.generateApiKey();
//     console.log('API KEY');
//     console.log(apiKey);

//     const params = {
//         minWithdrawable,
//         maxWithdrawable,
//         defaultDescription,
//         test: 'TEST123456'
//     };

//     const options = {
//         baseUrl: 'https://lnurl.bittip.org/lnurl',
//         encode: true,
//         shorten: true,
//         overrides: {
//             OverrideTest: 'AAABBBCCC'
//         },
//     };

//     //var result = await server.generateNewUrl(withdrawTag, params);
//     var encoded = await lnurl.createSignedUrl(apiKey, withdrawTag, params, options)
//     console.log('LNURL Withdraw ----------------------------------');
//     console.log(encoded);
//     return encoded;
// },