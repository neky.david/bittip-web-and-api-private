const { LightningBackend } = require('lnurl');
const lightning = require('./lightning.service');

class Backend extends LightningBackend {

    constructor() {
        //     var lndOptions = {
        //         hostname: process.env.LIGHTNING_API_URL,
        //         cert: process.env.LIGHTNING_API_LOCAL ? "tls.cert" : "services/lightning/tls.cert",
        //         macaroon: process.env.LIGHTNING_API_LOCAL ? "admin.macaroon" : "services/lightning/admin.macaroon",
        //     }

        //console.log('LNURL Backend Contructor');
        super('custom', null, {
            requiredOptions: [],
        });
    }

    checkOptions(options) {
        // console.log('LNURL - checkOptions');
        // console.log(options);
        // This is called by the constructor.
        // Throw an error if any problems are found with the given options.
    }

    getNodeUri() {
        console.log('LNURL - getNodeUri - ' + this.options.nodeUri);
        return Promise.resolve(this.options.nodeUri);
    }

    openChannel(remoteId, localAmt, pushAmt, makePrivate) {
        console.log('LNURL - openChannel not implemented');
        return Promise.reject('openChannel not implemented');
    }

    async payInvoice(payReq) {
        const paymentService = require('../../model/payments/payments.service');

        //return new Promise((resolve, reject) => {
        if (process.env.TESTING) {
            console.log('LNURL PAY INVOICE -------------------------------------');
            console.log(payReq);
        }

        //find Payment
        var payment = await paymentService.findByOutgoingPayReqAndCheckBalance(payReq)
        if (process.env.TESTING) { console.log(payment); }
        if (payment.isEmpty()) throw new Error("LNURL Pay Invoice - Payment not found"); //return reject(new Error("LNURL Pay Invoice - Payment not found"))

        //LN pay
        var lnPayment = await lightning.sendPaymentSync(payReq);

        if (process.env.TESTING) {
            console.log('lightning.sendPaymentSync');
            console.log(lnPayment);
        }

        lightning.checkInvoice(payReq)
            .then(invoice => {
                if (process.env.TESTING) {
                    console.log('lightning.checkInvoice');
                    console.log(invoice);
                }

                paymentService.payedOutgoing(payment, lnPayment.paymentRoute.total_amt_msat, lnPayment.paymentRoute.total_fees_msat,
                    invoice.description, null, lnPayment.paymentHash?.toString('base64'), invoice.paymentAddr.toString('base64'));
            })
            .catch(err => {
                console.log(err);
            })

        return lnPayment; //resolve(response);
        //})
    }

    addInvoice(amount, extra) {
        console.log('LNURL - addInvoice not implemented');
        return Promise.reject('addInvoice not implemented');
    }

    getInvoiceStatus(paymentHash) {
        console.log('LNURL - getInvoiceStatus not implemented');
        return Promise.reject('getInvoiceStatus not implemented');
    }
}

module.exports = Backend;