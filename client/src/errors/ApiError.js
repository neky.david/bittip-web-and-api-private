class ApiError extends Error {

    constructor(responseBody) {

        console.log(responseBody);

        if(responseBody && responseBody.error){
            super(responseBody.error.message);
            this.title = responseBody.error.title;
            this.type = responseBody.error.type ? responseBody.error.type : 'error', //warn, error, success
            this.viewable = responseBody.error.viewable;
        }
        else{
            super("Unexpected API error");
            this.viewable = false;
        }
        
        Object.setPrototypeOf(this, new.target.prototype);
        //Error.captureStackTrace(this);
    }
}

export default ApiError;