class UserError extends Error {

    constructor(message, title, type) {

        super(message);
        this.title = title;
        this.type = type ? type : 'error'; //warn, error, success
        this.viewable = true;

        Object.setPrototypeOf(this, new.target.prototype);
        //Error.captureStackTrace(this);
    }
}

export default UserError;