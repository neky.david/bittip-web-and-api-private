import ApiError from "./ApiError";
import { notify } from "@kyvg/vue3-notification";

function countDuration(message) {
    var minDuration = 3000;
    var durationFromMessage = message ? message.length * 150 : 0;
    return minDuration > durationFromMessage ? minDuration : durationFromMessage;
}

export default function (error) {

    if (error) {

        console.log(error);

        var title = "Unexpected error occured 😕"
        var message = "Try it again"
        var type = "error"

        if (error instanceof ApiError) {
            if (error.viewable) {
                console.log("VIEWABLE API Error: " + error.message);

                title = error.title;
                message = error.message;
                type = error.type;
            }
            else {
                console.log("API Error: " + error.message);
            }
        }
        else
            console.log("Unexpected error: " + error.message);


        notify({
            title: title,
            text: message,
            type: type, //warn, error, success
            duration: countDuration(error.message)
        });
    }

}