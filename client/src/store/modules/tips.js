import apiTips from '../../api/tips.api'

const state = {
    tip: {},
    tips: []
}

const getters = {
}

const actions = {
    async addPlatformTip({ commit }, { contentPublicId, platform, extraData }) {
        var tip = await apiTips.createPlatformTip(contentPublicId, platform, extraData);
        commit('setTip', tip);
    },
    async checkTipPayment({ commit, state }) {
        var payment = await apiTips.waitToPay(state.tip.id);
        commit('updatePayment', payment);
    },
    async loadMyTips({commit}, count) {
        var tips = await apiTips.myTips(count);
        commit('setTips', tips);
    }
}

const mutations = {
    setTip: (state, tip) => {
        state.tip = tip;
    },
    updatePayment: (state, payment) => {
        state.tip.payment = payment;
    },
    setTips: (state, tips) => {
        state.tips = tips;
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}