import apiProfiles from '../../api/profiles.api'

const state = {
    list: []
}

const getters = {
}

const actions = {
    async loadProfiles({ commit }) {
        var profiles = await apiProfiles.getMyProfiles();
        commit('setProfiles', profiles);
    },
    async disconnectProfile({ commit }, platformId) {
        var success = await apiProfiles.disconnect(platformId);
        if(success) commit('removeProfile', platformId);
    },
    async connectMedium({ commit }, integrationToken) {
        var newProfile = await apiProfiles.connectMedium(integrationToken);
        commit('addProfile', newProfile);
    }
}

const mutations = {
    setProfiles: (state, profiles) => {
        state.list = profiles;
    },
    removeProfile: (state, profileId) => {
        state.list = state.list.filter(p => p.id != profileId);
    },
    addProfile: (state, profile) => {
        if (profile)
            state.list.push(profile);
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}