import apiUsers from '../../api/users.api'
import apiAuth from '../../api/auth.api'

const state = {
    users: [],
    me: null
}

const getters = {
    isAuth(state) {
        return state.me != null && state.me.id > 0;
    }
}

const actions = {
    async loginLocal({ commit }, { email, password }) {
        var me = await apiAuth.login(email, password);
        commit('setMe', me);
    },
    async loadUsers({ commit }) {
        var users = await apiUsers.getAllUsers();
        commit('setUsers', users);
    },
    async auth(context) {
        if (!context.state.me) {
            var me;
            try {
                me = await apiUsers.getMe();
            }
            catch (err) {
                me = null;
            }
            context.commit('setMe', me);
        }
    },
    async logoutMe({ commit }) {
        let res = await apiAuth.logout();
        if (res) commit('setMe', null);
        return res;
    },
    async registerUser({ commit }, { name, email, password, password2 }) {
        let newUser = await apiAuth.register(name, email, password, password2);
        commit('setMe', newUser);
    }
}

const mutations = {
    setUsers: (state, users) => {
        state.users = users;
    },
    setMe: (state, me) => {
        state.me = me;
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}