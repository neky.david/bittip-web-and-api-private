import apiWallet from '../../api/wallets.api'

const state = {
    wallet: null,
    withdrawReq: null,
    withdrawPayment: null
}

const getters = {
}

const actions = {
    async loadWallet({ commit }, withStats= false) {
        var wallet = await apiWallet.getMyWallet(withStats);
        commit('setWallet', wallet);
    },
    async withdraw({commit}) {
        var withdrawReq = await apiWallet.withdrawMyWallet();
        commit('setWithdrawReq', withdrawReq);
    },
    async clearWithdrawAndWallet({commit}) {
        commit('setWithdrawReq', null);
        commit('setWallet', null);
    },
    async waitToWithdraw({commit}) {
        var payment = await apiWallet.waitToWithdrawPayed();
        console.log('Wait To Withdraw Payment');
        console.log(payment);
        commit('updateWalletWithdraw', payment.wallet.withdrawAmount);
        commit('setWithdrawPayment', payment);
    }
}

const mutations = {
    setWallet: (state, wallet) => {
        state.wallet = wallet;
    },
    setWithdrawReq: (state, withdrawReq) => {
        state.withdrawReq = withdrawReq;
    },
    updateWalletWithdraw: (state, withdrawAmount) => {
        state.wallet.withdrawAmount = withdrawAmount;
    },
    setWithdrawPayment: (state, payment) => {
        state.withdrawPayment = { 
            id: payment.id,
            amount: payment.amount,
            fees: payment.fees,
            payDate: payment.payDate
        };
    }
}

export default {
    state,
    getters,
    actions,
    mutations
}