import { createStore } from 'vuex'

import users from './modules/users'
import profiles from './modules/profiles'
import tips from './modules/tips'
import wallets from './modules/wallets'

export default createStore({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        users,
        profiles,
        tips,
        wallets
    }
})