function fallbackCopyTextToClipboard(text) {
    return new Promise((resolve, reject) => {
        var textArea = document.createElement("textarea");
        textArea.value = text;

        // Avoid scrolling to bottom
        textArea.style.top = "0";
        textArea.style.left = "0";
        textArea.style.position = "fixed";

        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();

        try {
            if (document.execCommand('copy'))
                resolve();
            else
                reject(new Error("Fallback: Copying text command was unsuccessful"));
        } catch (err) {
            reject(err);
        }

        document.body.removeChild(textArea);
    });
}

function copyToClipboard(text) {
    return new Promise((resolve, reject) => {
        if (!navigator.clipboard) {
            fallbackCopyTextToClipboard(text)
                .then(() => {
                    resolve();
                })
                .catch(err => {
                    reject(err);
                })
        }
        navigator.clipboard.writeText(text).then(function () {
            resolve();
        }, function (err) {
            reject(err);
        });
    });
}

export { copyToClipboard }
