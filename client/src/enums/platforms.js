const Platforms = Object.freeze({
    'YOUTUBE': 'YOUTUBE', 
    'MEDIUM': 'MEDIUM', 
    'STACKEXCHANGE': 'STACKEXCHANGE'
  });
  
  export default Platforms;