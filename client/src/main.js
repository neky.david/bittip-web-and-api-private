import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import errorHandler from './errors/error.handler';
import notification  from '@kyvg/vue3-notification'

var app = createApp(App)

app.use(notification);

app.use(store).use(router).mount('#app');

app.config.errorHandler = errorHandler;