import axios from './axios.api'

async function getMyWallet(withStats) {
    var wallet = null;
    var response = await axios.post('/wallets/myWallet', {
        withStats: withStats
    });
    wallet = response.data;
    return wallet;
}

async function withdrawMyWallet() {
    var response = await axios.post('/wallets/withdraw');
    return response.data;
}

async function waitToWithdrawPayed() {
    var response = await axios.post('/wallets/waitToWithdraw');
    return response.data;
}

export default {
    getMyWallet,
    withdrawMyWallet,
    waitToWithdrawPayed
}
