import axios from './axios.api'

async function getMyProfiles() {
    var profiles = [];
    var response = await axios.post('/profiles');
    profiles = response.data;
    return profiles;
}

async function disconnect(profileId) {
    var success = false;
    var response = await axios.post('/profiles/disconnect', {
        profileId: profileId
    });
    console.log(response.data);
    success = true;
    return success;
}

async function connectMedium(integrationToken) {
    var newProfile = null;
    var response = await axios.post('/medium/connect', {
        token: integrationToken
    });
    newProfile = response.data;
    return newProfile;
}

export default {
    getMyProfiles,
    connectMedium,
    disconnect
}