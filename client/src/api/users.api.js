import axios from './axios.api'

async function getAllUsers() {
    var users = [];
    var response = await axios.post('/users');
    users = response.data;
    return users;
}

async function getMe() {
    var me = null;
    var response = await axios.post('/users/me');
    me = response.data;
    return me;
}

export default {
    getAllUsers,
    getMe
}