import axios from './axios.api'

async function createPlatformTip(contentPublicId, platform, extraData) {
    var tip = [];
    var response = await axios.post('/tips/platform',
        {
            contentPublicId: contentPublicId,
            platform: platform,
            extraData: extraData
        });
    tip = response.data;
    return tip;
}

async function waitToPay(tipId) {
    var payment;
    var response = await axios.post('/tips/waitToPay',
        {
            tipId: tipId
        });

    payment = response.data;
    return payment;
}

async function myTips(count) {
    var tips = [];
    var response = await axios.post('/tips/myTips',
        {
            count: count
        });

    tips = response.data;
    return tips;
}

export default {
    createPlatformTip,
    waitToPay,
    myTips
}
