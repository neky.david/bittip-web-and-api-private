import axios from './axios.api'

async function login(email, password) {
    var res = await axios.post('/auth/login', {
        email: email,
        password: password
    });
    return res.data;
}

async function logout() {
    axios.post('/auth/logout');
    return true;
}

async function register(name, email, password, password2) {
    let res = await axios.post('/auth/register', {
        displayName: name,
        email: email,
        password: password,
        password2: password2
    });
    return res.data;
}

export default {
    login,
    logout,
    register
}