import axios from 'axios';
import ApiError from '../errors/ApiError';

const apiAxios = axios.create({
    baseURL: process.env.VUE_APP_API_URL,
    withCredentials: true
});

// Add a response interceptor
apiAxios.interceptors.response.use(
    function (response) {
        return response;
    },
    function (error) {
        console.log('API ERROR');
        console.log(error);
        return Promise.reject(new ApiError(error?.response?.data));
    }
);

export default apiAxios;