import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [{
    path: '/',
    name: 'Home',
    component: Home
},
{
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
        import('../views/Login.vue')
},
{
    path: '/profile',
    name: 'Profile',
    component: () => import('../views/Profile.vue'),
    children: [
        {
            path: '',
            name: 'ProfileOverview',
            component: () =>
                import('../views/ProfileOverview.vue')
        },
        {
            path: '/tips',
            name: 'ProfileTips',
            component: () =>
                import('../views/ProfileTips.vue')
        },
        {
            path: '/account',
            name: 'ProfileAccount',
            component: () =>
                import('../views/ProfileAccount.vue')
        },
        {
            path: '/FnF2mKR9oGetxWTgD64YVx',
            name: 'FnF2mKR9oGetxWTgD64YVx',
            component: () =>
                import('../views/ProfileWithdraw.vue')
        },
        {
            path: '/withdraw',
            name: 'ProfileWithdraw',
            component: () =>
                import('../views/ProfileSoon.vue')
        }
    ]
},
{
    path: '/register',
    name: 'Register',
    component: () =>
        import('../views/Register.vue')
},
{
    path: '/tip',
    name: 'Tip',
    component: () =>
        import('../views/Tip.vue')
}
]

const router = createRouter({
    history: createWebHistory(),
    //history: createWebHashHistory(),
    //mode: 'history',
    routes
})

import store from '../store';
var tryAuth = false;

var nonAuthPages = ['Home', 'Login', 'Tip', 'Register'];

router.beforeEach(async (to, from, next) => {
    if (!tryAuth) {
        await store.dispatch('auth');
        tryAuth = true;
    }
    if (!nonAuthPages.includes(to.name) && !store.getters.isAuth) next({ name: 'Login' });
    else next();
})

export default router