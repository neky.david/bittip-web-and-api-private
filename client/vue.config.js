const path = require('path');

module.exports = {
    //build assets to server folder (we dont need deploy client folder)
    outputDir: path.resolve(__dirname, 'build'),
    css: {
        loaderOptions: {
            sass: {
                //@/ is an alias to src/
                additionalData: `
                   @import "@/scss/variables"; 
                   @import "@/scss/bootstrap";
                   @import "@/scss/utils";
                   @import "@/scss/style";` //path to SCSS files
            }
        }
    }
};